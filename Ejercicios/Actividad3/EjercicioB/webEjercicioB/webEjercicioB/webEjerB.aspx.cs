﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using libPersona;
namespace webEjercicioB
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        #region "Metodos personalizados"
        private void Mensaje(string Texto)
        {
            this.lblMensaje.Text = Texto.Trim();
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.pnlRegistrar.Visible = true;
                this.pnlRegAdmin.Visible = true;
            }
        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            Mensaje(string.Empty);

            try
            {
                string strDocumento = txtDocumentoReg.Text;
                string strNombre = txtNombre.Text;
                string strApellido = txtApellido.Text;
                string strDireccion = txtDireccion.Text;
                string strTelefono = txtTelefono.Text;
                string strGenero = dlistGenero.Text;
                float fltSalario = Convert.ToSingle(txtSalario.Text);

                switch (this.rblRol.SelectedIndex + 1)
                {
                    case 1:  //Selecciona Administrativo
                        clsAdministrativo objAdmin = new clsAdministrativo(txtArea.Text, txtCargo.Text, txtExtension.Text);
                        objAdmin.Documento = strDocumento;
                        objAdmin.Nombre = strNombre;
                        objAdmin.Apellido = strApellido;
                        objAdmin.Direccion = strDireccion;
                        objAdmin.Telefono = strTelefono;
                        objAdmin.Genero = strGenero;
                        objAdmin.Salario = fltSalario;

                        if (!objAdmin.Registrar())
                        {
                            Mensaje("Error: " + objAdmin.Error);
                            objAdmin = null;
                            return;
                        }
                        break;
                    case 2: //Selecciona Docente
                        clsDocente objDoc = new clsDocente(txtTipo.Text, txtCategoria.Text, Convert.ToInt32(txtHoras.Text));
                        objDoc.Documento = strDocumento;
                        objDoc.Nombre = strNombre;
                        objDoc.Apellido = strApellido;
                        objDoc.Direccion = strDireccion;
                        objDoc.Telefono = strTelefono;
                        objDoc.Genero = strGenero;
                        objDoc.Salario = fltSalario;

                        if (!objDoc.Registrar())
                        {
                            Mensaje("Error: " + objDoc.Error);
                            objDoc = null;
                            return;
                        }
                        break;
                    default: //Selecciona Estudiante
                        clsEstudiante objEst = new clsEstudiante(txtCarne.Text, txtPrograma.Text);
                        objEst.Documento = strDocumento;
                        objEst.Nombre = strNombre;
                        objEst.Apellido = strApellido;
                        objEst.Direccion = strDireccion;
                        objEst.Telefono = strTelefono;
                        objEst.Genero = strGenero;
                        objEst.Salario = fltSalario;

                        if (!objEst.Registrar())
                        {
                            Mensaje("Error: " + objEst.Error);
                            objEst = null;
                            return;
                        }
                        break;
                }
                Mensaje("Registro exitoso.");
            }
            catch (Exception ex)
            {
                Mensaje(ex.Message);
                return;
            }
            
        }
        protected void rblAccion_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.pnlRegistrar.Visible = false;
            this.pnlConsultar.Visible = false;
            this.pnlResumen.Visible = false;

            switch (this.rblAccion.SelectedIndex + 1)
            {
                case 1:
                    this.pnlRegistrar.Visible = true;
                    break;
                case 2:
                    this.pnlConsultar.Visible = true;
                    break;
                default:
                    this.pnlResumen.Visible = true;
                    break;
            }
        }

        protected void rblRol_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.pnlRegAdmin.Visible = false;
            this.pnlRegDoc.Visible = false;
            this.pnlRegEst.Visible = false;

            switch (this.rblRol.SelectedIndex + 1)
            {
                case 1:
                    this.pnlRegAdmin.Visible = true;
                    break;
                case 2:
                    this.pnlRegDoc.Visible = true;
                    break;
                default:
                    this.pnlRegEst.Visible = true;
                    break;
            }
        }

        protected void btnResumen_Click(object sender, EventArgs e)
        {
            Mensaje(string.Empty);

            try
            {  
                switch (this.rblRol.SelectedIndex + 1)
                {
                    case 1:
                        clsAdministrativo objAdmin = new clsAdministrativo(null, null, null);

                        if (!objAdmin.Resumen())
                        {
                            Mensaje("Error: " + objAdmin.Error);
                            objAdmin = null;
                            return;
                        }
                        
                        DataTable dtAdmin = new DataTable();

                        DataColumn Documento = dtAdmin.Columns.Add("Documento");
                        DataColumn Nombre = dtAdmin.Columns.Add("Nombre");
                        DataColumn Apellido = dtAdmin.Columns.Add("Apellido");
                        DataColumn Genero = dtAdmin.Columns.Add("Genero");
                        DataColumn Direccion = dtAdmin.Columns.Add("Dirección");
                        DataColumn Telefono = dtAdmin.Columns.Add("Teléfono");
                        DataColumn Salario = dtAdmin.Columns.Add("Salario");
                        DataColumn Area = dtAdmin.Columns.Add("Area");
                        DataColumn Cargo = dtAdmin.Columns.Add("Cargo");
                        DataColumn Extension = dtAdmin.Columns.Add("Extensión");
                        
                        string[,] mResumAdmin = objAdmin.mDatos;
                        string[] vResumAdmin = new string[mResumAdmin.GetLength(1)];  //mResum.GetLength(1) : Columnas

                        for (int i = 0; i < mResumAdmin.GetLength(0); i++)  //El GetLength(0) : Filas
                        {
                            DataRow row = dtAdmin.NewRow();
                            for (int j = 0; j < vResumAdmin.Length; j++)
                            {
                                vResumAdmin[j] = mResumAdmin[i, j];
                            }
                            row.ItemArray = vResumAdmin;
                            dtAdmin.Rows.Add(row);
                        }
                        dtAdmin.AcceptChanges();

                        this.grvResAdmin.DataSource = dtAdmin;
                        this.grvResAdmin.DataBind();
                        break;
                    case 2:
                        clsDocente objDoc = new clsDocente(null, null, 0);

                        if (!objDoc.Resumen())
                        {
                            Mensaje("Error: " + objDoc.Error);
                            objDoc = null;
                            return;
                        }

                        DataTable dtDoc = new DataTable();

                        DataColumn DocumentoDoc = dtDoc.Columns.Add("Documento");
                        DataColumn NombreDoc = dtDoc.Columns.Add("Nombre");
                        DataColumn ApellidoDoc = dtDoc.Columns.Add("Apellido");
                        DataColumn GeneroDoc = dtDoc.Columns.Add("Genero");
                        DataColumn DireccionDoc = dtDoc.Columns.Add("Dirección");
                        DataColumn TelefonoDoc = dtDoc.Columns.Add("Teléfono");
                        DataColumn SalarioDoc = dtDoc.Columns.Add("Salario");
                        DataColumn TipoDoc = dtDoc.Columns.Add("Tipo");
                        DataColumn CategDoc = dtDoc.Columns.Add("Categoría");
                        DataColumn HorasDoc = dtDoc.Columns.Add("Horas");

                        string[,] mResum = objDoc.mDatos;
                        string[] vResum = new string[mResum.GetLength(1)];  //mResum.GetLength(1) : Columnas

                        for (int i = 0; i < mResum.GetLength(0); i++)  //El GetLength(0) : Filas
                        {
                            DataRow row = dtDoc.NewRow();
                            for (int j = 0; j < vResum.Length; j++)
                            {
                                vResum[j] = mResum[i, j];
                            }
                            row.ItemArray = vResum;
                            dtDoc.Rows.Add(row);
                        }
                        dtDoc.AcceptChanges();

                        this.grvResAdmin.DataSource = dtDoc;
                        this.grvResAdmin.DataBind();
                        break;
                    default:
                        clsEstudiante objEst = new clsEstudiante(null, null);

                        if (!objEst.Resumen())
                        {
                            Mensaje("Error: " + objEst.Error);
                            objEst = null;
                            return;
                        }

                        DataTable dtEst = new DataTable();

                        DataColumn DocumentoEst = dtEst.Columns.Add("Documento");
                        DataColumn NombreEst = dtEst.Columns.Add("Nombre");
                        DataColumn ApellidoEst = dtEst.Columns.Add("Apellido");
                        DataColumn GeneroEst = dtEst.Columns.Add("Genero");
                        DataColumn DireccionEst = dtEst.Columns.Add("Dirección");
                        DataColumn TelefonoEst = dtEst.Columns.Add("Teléfono");
                        DataColumn SalarioEst = dtEst.Columns.Add("Salario");
                        DataColumn CarnetEst = dtEst.Columns.Add("Carné");
                        DataColumn ProgramEst = dtEst.Columns.Add("Programa");

                        string[,] mResumEst = objEst.mDatos;
                        string[] vResumEst = new string[mResumEst.GetLength(1)];  //mResum.GetLength(1) : Columnas

                        for (int i = 0; i < mResumEst.GetLength(0); i++)  //El GetLength(0) : Filas
                        {
                            DataRow row = dtEst.NewRow();
                            for (int j = 0; j < vResumEst.Length; j++)
                            {
                                vResumEst[j] = mResumEst[i, j];
                            }
                            row.ItemArray = vResumEst;
                            dtEst.Rows.Add(row);
                        }
                        dtEst.AcceptChanges();

                        this.grvResAdmin.DataSource = dtEst;
                        this.grvResAdmin.DataBind();
                        break;
                }
            }
            catch (Exception ex)
            {
                Mensaje(ex.Message);
                return;
            }
        }

        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            Mensaje(string.Empty);

            try
            {
                string strDocumento = txtDocConsulta.Text;
                switch (this.rblRol.SelectedIndex + 1)
                {
                    case 1:
                        clsAdministrativo objAdmin = new clsAdministrativo(null, null, null);
                        objAdmin.Documento = strDocumento;

                        if (!objAdmin.Consultar())
                        {
                            Mensaje("Error: " + objAdmin.Error);
                            objAdmin = null;
                            return;
                        }

                        DataTable dtAdmin = new DataTable();

                        DataColumn DocumentoAdmin = dtAdmin.Columns.Add("Documento");
                        DataColumn NombreAdmin = dtAdmin.Columns.Add("Nombre");
                        DataColumn ApellidoAdmin = dtAdmin.Columns.Add("Apellido");
                        DataColumn GeneroAdmin = dtAdmin.Columns.Add("Genero");
                        DataColumn DireccionAdmin = dtAdmin.Columns.Add("Dirección");
                        DataColumn TelefonoAdmin = dtAdmin.Columns.Add("Teléfono");
                        DataColumn SalarioAdmin = dtAdmin.Columns.Add("Salario");
                        DataColumn AreaAdmin = dtAdmin.Columns.Add("Area");
                        DataColumn CargoAdmin = dtAdmin.Columns.Add("Cargo");
                        DataColumn ExtensionAdmin = dtAdmin.Columns.Add("Extensión");

                        DataRow row = dtAdmin.NewRow();
                        row.ItemArray = objAdmin.vConsulta;
                        dtAdmin.Rows.Add(row);
                        dtAdmin.AcceptChanges();

                        this.grvConsulta.DataSource = dtAdmin;
                        this.grvConsulta.DataBind();                        
                        break;
                    case 2:
                        clsDocente objDoc = new clsDocente(null, null, 0);
                        objDoc.Documento = strDocumento;

                        if (!objDoc.Consultar())
                        {
                            Mensaje("Error: " + objDoc.Error);
                            objDoc = null;
                            return;
                        }

                        DataTable dtDoc = new DataTable();

                        DataColumn DocumentoDoc = dtDoc.Columns.Add("Documento");
                        DataColumn NombreDoc = dtDoc.Columns.Add("Nombre");
                        DataColumn ApellidoDoc = dtDoc.Columns.Add("Apellido");
                        DataColumn GeneroDoc = dtDoc.Columns.Add("Genero");
                        DataColumn DireccionDoc = dtDoc.Columns.Add("Dirección");
                        DataColumn TelefonoDoc = dtDoc.Columns.Add("Teléfono");
                        DataColumn SalarioDoc = dtDoc.Columns.Add("Salario");
                        DataColumn TipoDoc = dtDoc.Columns.Add("Tipo");
                        DataColumn CategDoc = dtDoc.Columns.Add("Categoría");
                        DataColumn HorasDoc = dtDoc.Columns.Add("Horas");

                        DataRow rowDoc = dtDoc.NewRow();
                        rowDoc.ItemArray = objDoc.vConsulta;
                        dtDoc.Rows.Add(rowDoc);
                        dtDoc.AcceptChanges();

                        this.grvConsulta.DataSource = dtDoc;
                        this.grvConsulta.DataBind();
                        break;
                    default:
                        clsEstudiante objEst = new clsEstudiante(null, null);
                        objEst.Documento = strDocumento;

                        if (!objEst.Consultar())
                        {
                            Mensaje("Error: " + objEst.Error);
                            objEst = null;
                            return;
                        }

                        DataTable dtEst = new DataTable();

                        DataColumn DocumentoEst = dtEst.Columns.Add("Documento");
                        DataColumn NombreEst = dtEst.Columns.Add("Nombre");
                        DataColumn ApellidoEst = dtEst.Columns.Add("Apellido");
                        DataColumn GeneroEst = dtEst.Columns.Add("Genero");
                        DataColumn DireccionEst = dtEst.Columns.Add("Dirección");
                        DataColumn TelefonoEst = dtEst.Columns.Add("Teléfono");
                        DataColumn SalarioEst = dtEst.Columns.Add("Salario");
                        DataColumn CarnetEst = dtEst.Columns.Add("Carné");
                        DataColumn ProgramEst = dtEst.Columns.Add("Programa");

                        DataRow rowEst = dtEst.NewRow();
                        rowEst.ItemArray = objEst.vConsulta;
                        dtEst.Rows.Add(rowEst);
                        dtEst.AcceptChanges();

                        this.grvConsulta.DataSource = dtEst;
                        this.grvConsulta.DataBind();
                        break;
                }
            }
            catch (Exception ex)
            {
                Mensaje("Error: " + ex.Message);
                return;
            }
        }
    }
}