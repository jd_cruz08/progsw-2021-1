﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="webEjerB.aspx.cs" Inherits="webEjercicioB.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 80%;
        }
        .auto-style2 {
            width: 100%;
        }
        .auto-style3 {
            width: 226px;
            text-align: center;
        }
        .auto-style4 {
            height: 42px;
            font-size: x-large;
        }
        .auto-style5 {
            text-align: center;
        }
        .auto-style6 {
            text-align: center;
            height: 25px;
        }
        .auto-style7 {
            height: 23px;
        }
        .auto-style9 {
            text-align: center;
            height: 23px;
        }
        .auto-style10 {
            text-align: center;
            height: 46px;
        }
        .auto-style11 {
            text-align: left;
        }
        .auto-style12 {
            text-align: right;
        }
        .auto-style13 {
            width: 253px;
            text-align: right;
        }
        .auto-style15 {
            width: 253px;
            text-align: right;
            height: 26px;
        }
        .auto-style16 {
            text-align: left;
            height: 26px;
        }
        .auto-style17 {
            text-align: right;
            height: 26px;
        }
        .auto-style18 {
            text-align: right;
            width: 467px;
        }
        .auto-style19 {
            text-align: center;
            height: 42px;
        }
        .nuevoEstilo1 {
            width: 50%;
        }
        .auto-style21 {
            text-align: center;
            height: 38px;
        }
        .auto-style22 {
            text-align: center;
            height: 44px;
        }
        .auto-style24 {
            text-align: right;
            width: 40%;
            height: 35px;
        }
        .auto-style25 {
            height: 35px;
        }
        .auto-style27 {
            text-align: center;
            height: 122px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table align="center" class="auto-style1">
                <tr>
                    <td class="auto-style4" style="font-family: Arial, Helvetica, sans-serif;">
                        <table align="center" class="auto-style2">
                            <tr>
                                <td class="auto-style3" style="border: medium double #000000"><strong>Institución</strong></td>
                                <td style="border: medium double #000000"><strong>Comunidad</strong></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style27" style="font-family: Arial, Helvetica, sans-serif; font-size: medium"><strong>¿Qué acción desea realizar?<br />
                        </strong>
                        <asp:RadioButtonList ID="rblAccion" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="True" OnSelectedIndexChanged="rblAccion_SelectedIndexChanged">
                            <asp:ListItem Value="opcRegistrar" Selected="True">Registrar  </asp:ListItem>
                            <asp:ListItem Value="opcConsultar">Consultar  </asp:ListItem>
                            <asp:ListItem Value="opcResumen">Resumen  </asp:ListItem>
                        </asp:RadioButtonList>
                        <br />
                        <br />
                        <strong>¿A cuál rol se la desea realizar?</strong><br />
                                        <asp:RadioButtonList ID="rblRol" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="True" OnSelectedIndexChanged="rblRol_SelectedIndexChanged">
                                            <asp:ListItem Value="opcAdmin" Selected="True">Administrativo</asp:ListItem>
                                            <asp:ListItem Value="opcDoc">Docente</asp:ListItem>
                                            <asp:ListItem Value="opcEst">Estudiante</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                </tr>
                <tr>
                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: medium">
                        <asp:Panel ID="pnlRegistrar" runat="server" Visible="False">
                            <table class="auto-style2">
                                <tr>
                                    <td class="auto-style6">
                                        <table class="auto-style2">
                                            <tr>
                                                <td class="auto-style12" colspan="2"><strong>Documento</strong>:</td>
                                                <td class="auto-style11" colspan="2">
                                                    <asp:TextBox ID="txtDocumentoReg" runat="server" Width="50%"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style15" style="width: 25%"><strong>Nombre:</strong></td>
                                                <td class="auto-style16" style="width: 25%">
                                                    <asp:TextBox ID="txtNombre" runat="server" Width="80%"></asp:TextBox>
                                                </td>
                                                <td class="auto-style17" style="width: 25%"><strong>Apellido:</strong></td>
                                                <td class="auto-style16" style="width: 25%">
                                                    <asp:TextBox ID="txtApellido" runat="server" Width="80%"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style13" style="width: 25%"><strong>Genero:</strong></td>
                                                <td class="auto-style11" style="width: 25%">
                                                    <asp:DropDownList ID="dlistGenero" runat="server" Width="80%">
                                                        <asp:ListItem Value="Masculino">Masculino</asp:ListItem>
                                                        <asp:ListItem Value="Femenino">Femenino</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="auto-style12" style="width: 25%"><strong>Dirección:</strong></td>
                                                <td class="auto-style11" style="width: 25%">
                                                    <asp:TextBox ID="txtDireccion" runat="server" Width="80%"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style13" style="width: 25%"><strong>Teléfono:</strong></td>
                                                <td class="auto-style11" style="width: 25%">
                                                    <asp:TextBox ID="txtTelefono" runat="server" Width="80%"></asp:TextBox>
                                                </td>
                                                <td class="auto-style12" style="width: 25%"><strong>Salario:</strong></td>
                                                <td class="auto-style11" style="width: 25%">
                                                    <asp:TextBox ID="txtSalario" runat="server" TextMode="Number" Width="80%"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style7">
                                        <asp:Panel ID="pnlRegAdmin" runat="server" Visible="False">
                                            <table class="auto-style2">
                                                <tr>
                                                    <td class="auto-style18" style="width: 50%"><strong class="nuevoEstilo1">Área:</strong></td>
                                                    <td class="auto-style11" style="width: 50%">
                                                        <asp:TextBox ID="txtArea" runat="server" Width="50%"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style18" style="width: 50%"><strong>Cargo:</strong></td>
                                                    <td class="auto-style11" style="width: 50%">
                                                        <asp:TextBox ID="txtCargo" runat="server" Width="50%"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style18" style="width: 50%"><strong>Extensión:</strong></td>
                                                    <td class="auto-style11" style="width: 50%">
                                                        <asp:TextBox ID="txtExtension" runat="server" Width="50%"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="pnlRegDoc" runat="server" Visible="False">
                                            <table class="auto-style2">
                                                <tr>
                                                    <td class="auto-style18" style="width: 50%"><strong>Tipo:</strong></td>
                                                    <td>
                                                        <asp:TextBox ID="txtTipo" runat="server" Width="50%"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style18" style="width: 50%"><strong>Categoria:</strong></td>
                                                    <td>
                                                        <asp:TextBox ID="txtCategoria" runat="server" Width="50%"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style18" style="width: 50%"><strong>Horas:</strong></td>
                                                    <td>
                                                        <asp:TextBox ID="txtHoras" runat="server" Width="50%"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="pnlRegEst" runat="server" Visible="False">
                                            <table class="auto-style2">
                                                <tr>
                                                    <td class="auto-style18" style="width: 50%"><strong>Carné:</strong></td>
                                                    <td>
                                                        <asp:TextBox ID="txtCarne" runat="server" Width="50%"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style18" style="width: 50%"><strong>Programa:</strong></td>
                                                    <td>
                                                        <asp:TextBox ID="txtPrograma" runat="server" Width="50%"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style19">
                                        <asp:Button ID="btnRegistrar" runat="server" OnClick="btnRegistrar_Click" Text="Registrar" Width="104px" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: medium">
                        <asp:Panel ID="pnlConsultar" runat="server" Visible="False">
                            <table class="auto-style2">
                                <tr>
                                    <td class="auto-style24"><strong style="width: 50%">Documento:</strong></td>
                                    <td class="auto-style25" style="width: 50%">
                                        <asp:TextBox ID="txtDocConsulta" runat="server" Width="50%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style21" colspan="2">
                                        <asp:Button ID="btnConsultar" runat="server" Text="Consultar" OnClick="btnConsultar_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style10" colspan="2">
                                        <asp:GridView ID="grvConsulta" Font-Names="Arial" 
                                            Font-Size="1em" 
                                            CellPadding="7" CellSpacing="0" ForeColor="#333"
                                            runat="server" HorizontalAlign="Center">
                                            <EditRowStyle HorizontalAlign="Center" />
                                            <HeaderStyle BackColor="#989898" ForeColor="white" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style9" colspan="2">
                                        <asp:GridView ID="grvConsDoc" runat="server" CellPadding="7" CellSpacing="0" Font-Names="Arial" Font-Size="1em" ForeColor="#333" HorizontalAlign="Center">
                                            <EditRowStyle HorizontalAlign="Center" />
                                            <HeaderStyle BackColor="#989898" ForeColor="white" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style5" colspan="2">
                                        <asp:GridView ID="grvConsEst" runat="server" CellPadding="7" CellSpacing="0" Font-Names="Arial" Font-Size="1em" ForeColor="#333" HorizontalAlign="Center">
                                            <EditRowStyle HorizontalAlign="Center" />
                                            <HeaderStyle BackColor="#989898" ForeColor="white" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: medium">
                        <asp:Panel ID="pnlResumen" runat="server" Visible="False">
                            <table class="auto-style2">
                                <tr>
                                    <td class="auto-style22">
                                        <asp:Button ID="btnResumen" runat="server" Text="Generar Resumen" OnClick="btnResumen_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="grvResAdmin" runat="server" CellPadding="7" CellSpacing="0" Font-Names="Arial" Font-Size="1em" ForeColor="#333" HorizontalAlign="Center">
                                            <EditRowStyle HorizontalAlign="Center" />
                                            <HeaderStyle BackColor="#989898" ForeColor="white" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5" style="font-family: Arial, Helvetica, sans-serif; font-size: medium">
                        <asp:Label ID="lblMensaje" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
