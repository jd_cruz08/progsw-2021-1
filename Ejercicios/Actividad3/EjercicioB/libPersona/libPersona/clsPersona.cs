﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using libInstitucion;
using System.IO;

namespace libPersona
{
    public abstract class clsPersona : libInstitucion.clsInstitucion
    {
        #region "Atributos"
        protected string strDocumento;
        protected string strApellido;
        protected string strTelefono;
        protected string strGenero;
        protected float fltSalario;
        protected string[] ansConsulta;
        protected string[,] matrizDatos;
        #endregion

        #region "Propiedades"
        public string Documento
        {
            set { strDocumento = value; }
            get { return strDocumento; }
        }
        public string Apellido
        {
            set { strApellido = value; }
            get { return strApellido; }
        }
        public string Telefono
        {
            set { strTelefono = value; }
            get { return strTelefono; }
        }
        public string Genero
        {
            set { strGenero = value; }
            get { return strGenero; }
        }
        public float Salario
        {
            set { fltSalario = value; }
            get { return fltSalario; }
        }
        public string[] vConsulta
        {
            get { return ansConsulta; }
        }
        public string[,] mDatos
        {
            get { return matrizDatos; }
        }
        #endregion
    }

    public class clsAdministrativo : clsPersona
    {
        #region "Atributos"
        private string strArea;
        private string strCargo;
        private string strExtension;
        #endregion

        #region "Constructor"
        public clsAdministrativo(string Area, string Cargo, string Extension)
        {
            strArea = Area;
            strCargo = Cargo;
            strExtension = Extension;
            //Heredados
            strNombre = string.Empty;
            strDireccion = string.Empty;
            strDocumento = string.Empty;
            strApellido = string.Empty;
            strTelefono = string.Empty;
            strGenero = string.Empty;
            fltSalario = 0;
            strError = string.Empty;
            matrizDatos = new string[0,0];
            ansConsulta = new string[0];
        }
        #endregion

        #region "Propiedades"
        public string Area
        {
            set { strArea = value; }
            get { return strArea; }
        }
        public string Cargo
        {
            set { strCargo = value; }
            get { return strCargo; }
        }
        public string Extension
        {
            set { strExtension = value; }
            get { return strExtension; }
        }

        
        #endregion

        #region "Métodos Privados"
        private bool Validar()
        {
            if (!strDocumento.All(char.IsDigit))
            {
                strError = "Documento no válido, debe contener sólo números.";
                return false;
            }
            if (!strTelefono.All(char.IsDigit))
            {
                strError = "Teléfono no válido, debe contener sólo números.";
                return false;
            }
            if (!strExtension.All(char.IsDigit))
            {
                strError = "Extensión no válida, debe contener sólo números.";
                return false;
            }
            if (strTelefono.Length != 7 && strTelefono.Length != 10)
            {
                strError = "Lóngitud de teléfono no válida, debe ser 7 o 10 digitos.";
                return false;
            }
            if (fltSalario < 0)
            {
                strError = "Salario no válido, no puede ser un valor negativo.";
                return false;
            }
            return true;
        }
        private bool validarConsulta()
        {
            if (!strDocumento.All(char.IsDigit))
            {
                strError = "Documento no válido, debe contener sólo números.";
                return false;
            }
            return true;
        }
        #endregion

        #region "Métodos Públicos"
        public override bool Consultar() //Está pensado para realizar una consulta acorde al nro. de documento del administrativo, docente o estudiante
        {
            if (!validarConsulta())
                return false;

            try
            {
                string strPath = AppDomain.CurrentDomain.BaseDirectory + @"Administrativo.txt";
                int intCant = File.ReadAllLines(strPath).Length;
                if (intCant <= 0)
                    return true;

                string strLinea, strDoc;
                string[] vectorLinea;

                StreamReader Archivo = new StreamReader(@strPath);
                while ((strLinea = Archivo.ReadLine()) != null)
                {
                    vectorLinea = strLinea.Split(';');
                    strDoc = vectorLinea[0];

                    if (strDoc == strDocumento)
                    {
                        ansConsulta = vectorLinea;
                        break;
                    }
                }
                Archivo.Close();
                return true;
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return false;
            }
        }

        public override bool Registrar() //Está pensado para grabar en un archivo plano de tipo txt o xml la información capturada de un administrativo, docente o estudiante.
        {
            if (!Validar())
                return false;

            string strPath = AppDomain.CurrentDomain.BaseDirectory + @"Administrativo.txt";
            string strCadena = strDocumento + ";" + strNombre + ";" + strApellido + ";" +
                               strGenero + ";" + strDireccion + ";" + strTelefono + ";" +
                               fltSalario + ";" + strArea + ";" + strCargo + ";" + strExtension;

            try
            {
                using (StreamWriter Writer = new StreamWriter(strPath, true))
                {
                    Writer.WriteLine(strCadena);
                }
                return true;
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return false;
            }
        }

        public override bool Resumen()  //Muestra toda la información existente de todos los administrativos, docentes o estudiantes en el formulario
        {
            try
            {
                string strPath = AppDomain.CurrentDomain.BaseDirectory + @"Administrativo.txt";
                int intCant = File.ReadAllLines(strPath).Length;
                if (intCant <= 0)
                    return true;
                string strLinea;
                string[] vectorLinea;
                matrizDatos = new string[intCant,10];

                StreamReader Archivo = new StreamReader(@strPath);

                int j = 0;
                while ((strLinea = Archivo.ReadLine()) != null)
                {
                    vectorLinea = strLinea.Split(';');
                    for (int i = 0; i < vectorLinea.Length; i++)
                    {
                        matrizDatos[j,i] = vectorLinea[i];
                    }
                    
                    j++;
                }
                return true;
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return false;
            }
        }
        #endregion
    }

    public class clsDocente : clsPersona
    {
        #region "Atributos"
        private string strTipo;
        private string strCategoria;
        private int intHoras;
        #endregion

        #region "Constructor"
        public clsDocente(string Tipo, string Categoria, int Horas)
        {
            strTipo = Tipo;
            strCategoria = Categoria;
            intHoras = Horas;
            //Heredados
            strNombre = string.Empty;
            strDireccion = string.Empty;
            strDocumento = string.Empty;
            strApellido = string.Empty;
            strTelefono = string.Empty;
            strGenero = string.Empty;
            fltSalario = 0;
            strError = string.Empty;
            ansConsulta = new string[0];
            matrizDatos = new string[0, 0];
        }
        #endregion

        #region "Propiedades"
        public string Tipo
        {
            set { strTipo = value; }
            get { return strTipo; }
        }
        public string Categoria
        {
            set { strCategoria = value; }
            get { return strCategoria; }
        }
        public int Horas
        {
            set { intHoras = value; }
            get { return intHoras; }
        }
        #endregion

        #region "Métodos Privados"
        private bool Validar()
        {
            if (!strDocumento.All(char.IsDigit))
            {
                strError = "Documento no válido, debe contener sólo números.";
                return false;
            }
            if (!strTelefono.All(char.IsDigit))
            {
                strError = "Teléfono no válido, debe contener sólo números.";
                return false;
            }
            if (strTelefono.Length != 7 && strTelefono.Length != 10)
            {
                strError = "Lóngitud de teléfono no válida, debe ser 7 o 10 digitos.";
                return false;
            }
            if (fltSalario < 0)
            {
                strError = "Salario no válido, no puede ser un valor negativo.";
                return false;
            }
            if (intHoras < 0)
            {
                strError = "Cantidad de horas no puede ser negativa.";
                return false;
            }
            return true;
        }

        private bool validarConsulta()
        {
            if (!strDocumento.All(char.IsDigit))
            {
                strError = "Documento no válido, debe contener sólo números.";
                return false;
            }
            return true;
        }
        #endregion

        #region "Métodos Públicos"
        public override bool Consultar()
        {
            if (!validarConsulta())
                return false;

            try
            {
                string strPath = AppDomain.CurrentDomain.BaseDirectory + @"Docente.txt";
                int intCant = File.ReadAllLines(strPath).Length;
                if (intCant <= 0)
                    return true;

                string strLinea, strDoc;
                string[] vectorLinea;

                StreamReader Archivo = new StreamReader(@strPath);
                while ((strLinea = Archivo.ReadLine()) != null)
                {
                    vectorLinea = strLinea.Split(';');
                    strDoc = vectorLinea[0];

                    if (strDoc == strDocumento)
                    {
                        ansConsulta = vectorLinea;
                        break;
                    }
                }
                Archivo.Close();
                return true;
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return false;
            }
        }

        public override bool Registrar()
        {
            if (!Validar())
                return false;

            string strPath = AppDomain.CurrentDomain.BaseDirectory + @"Docente.txt";
            string strCadena = strDocumento + ";" + strNombre + ";" + strApellido + ";" +
                               strGenero + ";" + strDireccion + ";" + strTelefono + ";" +
                               fltSalario + ";" + strTipo + ";" + strCategoria + ";" + intHoras;

            try
            {
                using (StreamWriter Writer = new StreamWriter(strPath, true))
                {
                    Writer.WriteLine(strCadena);
                }
                return true;
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return false;
            }
        }

        public override bool Resumen()
        {
            try
            {
                string strPath = AppDomain.CurrentDomain.BaseDirectory + @"Docente.txt";
                int intCant = File.ReadAllLines(strPath).Length;
                if (intCant <= 0)
                    return true;
                string strLinea;
                string[] vectorLinea;
                matrizDatos = new string[intCant, 10];

                StreamReader Archivo = new StreamReader(@strPath);

                int j = 0;
                while ((strLinea = Archivo.ReadLine()) != null)
                {
                    vectorLinea = strLinea.Split(';');
                    for (int i = 0; i < vectorLinea.Length; i++)
                    {
                        matrizDatos[j, i] = vectorLinea[i];
                    }

                    j++;
                }
                return true;
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return false;
            }
        }
        #endregion
    }

    public class clsEstudiante : clsPersona
    {
        #region "Atributos"
        private string strCarnet;
        private string strPrograma;
        #endregion

        #region "Constructor"
        public clsEstudiante(string Carnet, string Programa)
        {
            strCarnet = Carnet;
            strPrograma = Programa;
            //Heredados
            strNombre = string.Empty;
            strDireccion = string.Empty;
            strDocumento = string.Empty;
            strApellido = string.Empty;
            strTelefono = string.Empty;
            strGenero = string.Empty;
            fltSalario = 0;
            strError = string.Empty;
            ansConsulta = new string[0];
            matrizDatos = new string[0, 0];
        }
        #endregion

        #region "Propiedades"
        public string Carnet
        {
            set { strCarnet = value; }
            get { return strCarnet; }
        }
        public string Programa
        {
            set { strPrograma = value; }
            get { return strPrograma; }
        }
        #endregion

        #region "Métodos Privados"
        private bool Validar()
        {
            if (!strDocumento.All(char.IsDigit))
            {
                strError = "Documento no válido, debe contener sólo números.";
                return false;
            }
            if (!strTelefono.All(char.IsDigit))
            {
                strError = "Teléfono no válido, debe contener sólo números.";
                return false;
            }
            if (!strCarnet.All(char.IsDigit))
            {
                strError = "Carné no válido, debe contener sólo números.";
                return false;
            }
            if (strTelefono.Length != 7 && strTelefono.Length != 10)
            {
                strError = "Lóngitud de teléfono no válida, debe ser 7 o 10 digitos.";
                return false;
            }
            if (fltSalario < 0)
            {
                strError = "Salario no válido, no puede ser un valor negativo.";
                return false;
            }
            return true;
        }

        private bool validarConsulta()
        {
            if (!strDocumento.All(char.IsDigit))
            {
                strError = "Documento no válido, debe contener sólo números.";
                return false;
            }
            return true;
        }
        #endregion

        #region "Métodos Públicos"
        public override bool Consultar()
        {
            if (!validarConsulta())
                return false;

            try
            {
                string strPath = AppDomain.CurrentDomain.BaseDirectory + @"Estudiante.txt";
                int intCant = File.ReadAllLines(strPath).Length;
                if (intCant <= 0)
                    return true;

                string strLinea, strDoc;
                string[] vectorLinea;

                StreamReader Archivo = new StreamReader(@strPath);
                while ((strLinea = Archivo.ReadLine()) != null)
                {
                    vectorLinea = strLinea.Split(';');
                    strDoc = vectorLinea[0];

                    if (strDoc == strDocumento)
                    {
                        ansConsulta = vectorLinea;
                        break;
                    }
                }
                Archivo.Close();
                return true;
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return false;
            }
        }

        public override bool Registrar()
        {
            if (!Validar())
                return false;

            string strPath = AppDomain.CurrentDomain.BaseDirectory + @"Estudiante.txt";
            string strCadena = strDocumento + ";" + strNombre + ";" + strApellido + ";" +
                               strGenero + ";" + strDireccion + ";" + strTelefono + ";" +
                               fltSalario + ";" + strCarnet + ";" + strPrograma;

            try
            {
                using (StreamWriter Writer = new StreamWriter(strPath, true))
                {
                    Writer.WriteLine(strCadena);
                }
                return true;
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return false;
            }
        }

        public override bool Resumen()
        {
            try
            {
                string strPath = AppDomain.CurrentDomain.BaseDirectory + @"Estudiante.txt";
                int intCant = File.ReadAllLines(strPath).Length;
                if (intCant <= 0)
                    return true;
                string strLinea;
                string[] vectorLinea;
                matrizDatos = new string[intCant, 9];

                StreamReader Archivo = new StreamReader(@strPath);

                int j = 0;
                while ((strLinea = Archivo.ReadLine()) != null)
                {
                    vectorLinea = strLinea.Split(';');
                    for (int i = 0; i < vectorLinea.Length; i++)
                    {
                        matrizDatos[j, i] = vectorLinea[i];
                    }

                    j++;
                }
                return true;
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return false;
            }
        }
        #endregion
    }
}
