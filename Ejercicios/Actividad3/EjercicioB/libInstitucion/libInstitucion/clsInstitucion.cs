﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libInstitucion
{
    public abstract class clsInstitucion
    {
        #region "Atributos"
        protected string strNombre;
        protected string strDireccion;
        protected string strError;
        #endregion

        #region "Propiedades"
        public string Nombre
        {
            set { strNombre = value; }
            get { return strNombre; }
        }
        public string Direccion
        {
            set { strDireccion = value; }
            get { return strDireccion; }
        }
        public string Error
        {
            get { return strError; }
        }
        #endregion

        #region "Métodos Públicos"
        public abstract bool Registrar();
        public abstract bool Consultar();
        public abstract bool Resumen();
        #endregion
    }
}
