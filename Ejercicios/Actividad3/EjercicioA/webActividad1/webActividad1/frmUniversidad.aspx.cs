﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using libEmpleado;
using libMatricula;

namespace webActividad1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        #region "Metodos Personalizados"
        private void Mensaje(string Texto)
        {
            this.lblMsj.Text = Texto.Trim();
        }

        private void limpiarText(int x)
        {
            foreach (Control cltFrm in this.Form.Controls)         //Recorre todos los elementos del formulario
            {
                if (cltFrm is TextBox && x == 1)                   //Se detiene en aquellos tipo TextBox (Exteriores a los paneles)
                {
                    ((TextBox)cltFrm).Text = string.Empty;
                }

                if (cltFrm is Panel)                               // Se detiene en aquellos tipo Panel
                {
                    foreach (Control cltPanel1 in cltFrm.Controls) // Recorre todos los elementos del Panel
                    {
                        if (cltPanel1 is TextBox && (x == 1 || x == 2))  //Se detiene en los TextBox del panel TipoPersona
                        {
                            ((TextBox)cltPanel1).Text = string.Empty;
                        }

                        if (cltPanel1 is Panel)             // Recorre todos los elementos del Panel TipoMatricula
                        {
                            foreach (Control cltPanel2 in cltPanel1.Controls)
                            {
                                if (cltPanel2 is TextBox && (x == 1 || x == 3))  //Se detiene en los TextBox del panel TipoMatricula
                                {
                                    ((TextBox)cltPanel2).Text = string.Empty;
                                }
                            }
                        }
                    }
                }
            }

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.pnlEstudiante.Visible = true;
                this.pnlPregrado.Visible = true;
            }
        }

        protected void Button1_Click(object sender, EventArgs e) //Grabar
        {
            Mensaje(string.Empty);

            try
            {
                string strDocumento = txtDocumento.Text;
                string strNombre = txtNombre.Text;
                string strFecha = txtFechaNac.Text;
                string strTelefono = txtTelefono.Text;

                switch (rblTipoPersona.SelectedIndex + 1)
                {
                    case 1:
                        string strFacultad = txtFacultad.Text;
                        switch (rblMatricula.SelectedIndex + 1)
                        {
                            case 1: //Estudiante - Pregrado
                                clsPregrado objEst = new clsPregrado(txtPrograma.Text, txtCarnet.Text);

                                objEst.Documento = strDocumento;
                                objEst.Nombre = strNombre;
                                objEst.FechaNac = Convert.ToDateTime(strFecha);
                                objEst.Telefono = strTelefono;
                                objEst.Facultad = strFacultad;

                                if (!objEst.Grabar())
                                {
                                    Mensaje("Error: " + objEst.Error);
                                    objEst = null;
                                    return;
                                }
                                break;
                            default:  //Estudiante - Extension
                                clsExtension objExt = new clsExtension(txtCurso.Text);

                                objExt.Documento = strDocumento;
                                objExt.Nombre = strNombre;
                                objExt.FechaNac = Convert.ToDateTime(strFecha);
                                objExt.Telefono = strTelefono;
                                objExt.Facultad = strFacultad;

                                if (!objExt.Grabar())
                                {
                                    Mensaje("Error: " + objExt.Error);
                                    objExt = null;
                                    return;
                                }
                                break;
                        }
                        break;
                    default:  //Empleado
                        clsEmpleado objEmp = new clsEmpleado();

                        objEmp.Documento = strDocumento;
                        objEmp.Nombre = strNombre;
                        objEmp.FechaNac = Convert.ToDateTime(strFecha);
                        objEmp.Telefono = strTelefono;
                        objEmp.Cargo = txtCargo.Text;
                        objEmp.Salario = Convert.ToInt32(txtSalario.Text);

                        if (!objEmp.Grabar())
                        {
                            Mensaje("Error: " + objEmp.Error);
                            objEmp = null;
                            return;
                        }
                        break;
                }
                Mensaje("Registro exitoso.");
            }
            catch (Exception ex)
            {
                Mensaje("Error, " + ex.Message);
                return;
            }
        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            limpiarText(1);
            Mensaje(string.Empty);
        }

        protected void rblTipoPersona_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.pnlEstudiante.Visible = false;
            this.pnlEmpleado.Visible = false;
            limpiarText(2);
            switch (this.rblTipoPersona.SelectedIndex + 1)
            {
                case 1:
                    this.pnlEstudiante.Visible = true;
                    this.txtFacultad.Focus();
                    break;
                default:
                    this.pnlEmpleado.Visible = true;
                    this.txtCargo.Focus();
                    break;
            }
        }

        protected void rblMatricula_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.pnlPregrado.Visible = false;
            this.pnlExtension.Visible = false;
            limpiarText(3);
            switch (this.rblMatricula.SelectedIndex + 1)
            {
                case 1:
                    this.pnlPregrado.Visible = true;
                    this.txtPrograma.Focus();
                    break;
                default:
                    this.pnlExtension.Visible = true;
                    this.txtCargo.Focus();
                    break;
            }
        }
    }
}