﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmUniversidad.aspx.cs" Inherits="webActividad1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 80%;
        }
        .auto-style2 {
            width: 100%;
            border: 1px solid #000000;
        }
        .auto-style3 {
            width: 213px;
            text-align: center;
        }
        .auto-style4 {
            font-size: large;
        }
        .auto-style5 {
            width: 90%;
            margin-right: 0px;
        }
        .auto-style6 {
            height: 23px;
        }
        .auto-style7 {
            width: 325px;
        }
        .auto-style9 {
            text-align: right;
            width: 174px;
        }
        .auto-style10 {
            height: 25px;
            text-align: center;
        }
        .auto-style11 {
            height: 19px;
        }
        .auto-style12 {
            text-align: center;
        }
        .auto-style13 {
            text-align: left;
            width: 435px;
        }
        .auto-style14 {
            width: 341px;
        }
        .auto-style15 {
            width: 341px;
            text-align: right;
        }
        .auto-style16 {
            width: 260px;
            text-align: right;
        }
        .auto-style17 {
            width: 339px;
            text-align: right;
        }
        .auto-style18 {
            text-align: right;
            width: 194px;
        }
        .auto-style19 {
            width: 221px;
        }
        .auto-style20 {
            color: #3333FF;
        }
        .auto-style22 {
            width: 435px;
        }
        .auto-style23 {
            height: 26px;
            text-align: center;
        }
        .auto-style24 {
            text-align: center;
            height: 53px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table align="center" class="auto-style1" style="font-family: 'Bookman Old Style'; font-size: medium">
                <tr>
                    <td>
                        <table align="center" class="auto-style2">
                            <tr>
                                <td class="auto-style3" style="font-family: arial, Helvetica, sans-serif; font-size: x-large; border-right-style: double; border-right-width: thin; border-right-color: #FF0000;"><span class="auto-style4"><strong>Universidad</strong></span><strong><br class="auto-style4" />
                                    </strong><span class="auto-style4"><strong>Los Alpes</strong></span></td>
                                <td class="auto-style4" style="font-family: arial, Helvetica, sans-serif; font-size: x-large"><strong>&nbsp;&nbsp;&nbsp; Registro de Personas</strong></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table align="center" class="auto-style5">
                            <tr>
                                <td class="auto-style9"><strong>Documento:</strong></td>
                                <td class="auto-style19">
                                    <asp:TextBox ID="txtDocumento" runat="server" Width="150px"></asp:TextBox>
                                </td>
                                <td class="auto-style18"><strong>Fecha de Nacimiento:</strong></td>
                                <td class="auto-style7">
                                    <asp:TextBox ID="txtFechaNac" runat="server" TextMode="DateTime" Width="150px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style9"><strong>Nombre</strong></td>
                                <td class="auto-style19">
                                    <asp:TextBox ID="txtNombre" runat="server" Width="150px"></asp:TextBox>
                                </td>
                                <td class="auto-style18"><strong>Teléfono:</strong></td>
                                <td class="auto-style7">
                                    <asp:TextBox ID="txtTelefono" runat="server" Width="150px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style11"></td>
                </tr>
                <tr>
                    <td class="auto-style10">
                        <strong>
                        <asp:RadioButtonList ID="rblTipoPersona" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="auto-style20" OnSelectedIndexChanged="rblTipoPersona_SelectedIndexChanged">
                            <asp:ListItem Selected="True" Value="opcEst">Estudiante</asp:ListItem>
                            <asp:ListItem Value="opcEmpleado">Empleado</asp:ListItem>
                        </asp:RadioButtonList>
                        </strong>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style6"></td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="pnlEstudiante" runat="server" BackColor="#FFCC99" Visible="False">
                            <table align="center" class="auto-style1">
                                <tr>
                                    <td class="auto-style15"><strong>Facultad:&nbsp; </strong></td>
                                    <td class="auto-style13">
                                        <asp:TextBox ID="txtFacultad" runat="server" Width="190px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style14">&nbsp;</td>
                                    <td class="auto-style22">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style12" colspan="2"><strong>¿Estudiante de ...?</strong></td>
                                </tr>
                                <tr>
                                    <td class="auto-style12" colspan="2">
                                        <asp:RadioButtonList ID="rblMatricula" runat="server" AutoPostBack="True" OnSelectedIndexChanged="rblMatricula_SelectedIndexChanged" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                            <asp:ListItem Selected="True" Value="opcPregrado">Pregrado</asp:ListItem>
                                            <asp:ListItem Value="opcExten">Extensión</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Panel ID="pnlPregrado" runat="server" BackColor="#CCFFFF" Visible="False">
                                            <table align="center" class="auto-style1">
                                                <tr>
                                                    <td class="auto-style16"><strong>Programa:</strong></td>
                                                    <td>
                                                        <asp:TextBox ID="txtPrograma" runat="server" Width="150px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style16"><strong>Carnet:</strong></td>
                                                    <td>
                                                        <asp:TextBox ID="txtCarnet" runat="server" Width="150px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Panel ID="pnlExtension" runat="server" BackColor="#CCCCFF" Visible="False">
                                            <table align="center" class="auto-style1">
                                                <tr>
                                                    <td class="auto-style16"><strong>Curso:</strong></td>
                                                    <td>
                                                        <asp:TextBox ID="txtCurso" runat="server" Width="150px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style6"></td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="pnlEmpleado" runat="server" BackColor="#CCFF99" Visible="False">
                            <table align="center" class="auto-style1">
                                <tr>
                                    <td class="auto-style17"><strong>Cargo:</strong></td>
                                    <td>
                                        <asp:TextBox ID="txtCargo" runat="server" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style17"><strong>Salario:</strong></td>
                                    <td>
                                        <asp:TextBox ID="txtSalario" runat="server" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style23">
                        <asp:Label ID="lblMsj" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style24">
                        <asp:Button ID="Button1" runat="server" BackColor="#33CC33" Font-Bold="True" Font-Names="Arial Rounded MT Bold" Font-Size="Medium" Height="32px" OnClick="Button1_Click" Text="Grabar" Width="104px" />
&nbsp;&nbsp;
                        <asp:Button ID="btnLimpiar" runat="server" BackColor="#33CCFF" Font-Bold="True" Font-Names="Arial Rounded MT Bold" Font-Size="Medium" Height="32px" OnClick="btnLimpiar_Click" Text="Limpiar" Width="104px" />
                    </td>
                </tr>
                </table>
        </div>
    </form>
</body>
</html>
