﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libPersona
{
    public abstract class clsPersona
    {
        #region "Atributos"
        protected string strDocumento;
        protected string strNombres;
        protected DateTime dtmFechaNac;
        protected string strTelefono;
        protected string strError;
        #endregion

        #region "Propiedades"
        public string Documento
        {
            set { strDocumento = value; }
            get { return strDocumento; }
        }
        public string Nombre
        {
            set { strNombres = value; }
            get { return strNombres; }
        }
        public DateTime FechaNac
        {
            set { dtmFechaNac = value; }
            get { return dtmFechaNac; }
        }
        public string Telefono
        {
            set { strTelefono = value; }
            get { return strTelefono; }
        }
        public string Error
        {
            get { return strError; }
        }
        #endregion

        #region "Métodos Públicos"
        public abstract bool Grabar();
        #endregion
    }
}
