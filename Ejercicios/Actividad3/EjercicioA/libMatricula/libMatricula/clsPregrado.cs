﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using libEstudiante;

namespace libMatricula
{
    public class clsPregrado : clsEstudiante
    {
        #region "Atributos"
        private string strPrograma;
        private string strCarnet;
        #endregion

        #region "Constructor"
        /// <summary>
        /// Para crear el objeto tipo Pregrado
        /// </summary>
        /// <param name="Programa">Nombre del programa</param>
        /// <param name="Carnet">Carnet del estudiante</param>
        public clsPregrado(string Programa, string Carnet)
        {
            strPrograma = Programa;
            strCarnet = Carnet;
            //Heredados de Persona
            strDocumento = string.Empty;
            strNombres = string.Empty;
            dtmFechaNac = DateTime.Today;
            strTelefono = string.Empty;
            strError = string.Empty;
            //Heredado de Estudiante
            strFacultad = string.Empty;
        }
        #endregion

        #region "Propiedades"
        public string Programa
        {
            set { strPrograma = value; }
            get { return strPrograma; }
        }
        public string Carnet
        {
            set { strCarnet = value; }
            get { return strCarnet; }
        }
        #endregion

        #region "Métodos Privados"
        private bool Validar()
        {
            if (dtmFechaNac == DateTime.Today || dtmFechaNac > DateTime.Today)
            {
                strError = "Fecha de nacimiento no válida. Por favor ingrese nuevamente.";
                return false;
            }
            if (!strDocumento.All(char.IsDigit) || !strTelefono.All(char.IsDigit) || !strCarnet.All(char.IsDigit))
            {
                strError = "El documento, teléfono y carnet debe contener solo números. Intente nuevamente";
                return false;
            }
            return true;
        }
        #endregion

        #region "Métodos Públicos"
        public override bool Grabar()
        {
            if (!Validar())
                return false;

            string strPath = AppDomain.CurrentDomain.BaseDirectory + @"Pregrado.txt";
            string strCadena = strDocumento + ";" + strNombres + ";" + 
                               dtmFechaNac  + ";" + strTelefono + ";" + 
                               strFacultad  + ";" + strPrograma + ";" + strCarnet;

            try
            {
                using (StreamWriter Writer = new StreamWriter(strPath, true))
                {
                    Writer.WriteLine(strCadena);
                }
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return false;
            }
            return true;
        }
        #endregion
    }
    public class clsExtension : clsEstudiante
    {
        #region "Atributos"
        private string strCurso;
        #endregion

        #region "Constructor"
        /// <summary>
        /// Para crear el objeto tipo Extensión
        /// </summary>
        /// <param name="Curso">Curso de extensión en que está inscrito</param>
        public clsExtension(string Curso)
        {
            strCurso = Curso;
            //Heredados
            strDocumento = string.Empty;
            strNombres = string.Empty;
            dtmFechaNac = DateTime.Today;
            strTelefono = string.Empty; ;
            strError = string.Empty;
        }
        #endregion

        #region "Propiedades"
        public string Curso
        {
            set { strCurso = value; }
            get { return strCurso; }
        }
        #endregion

        #region "Métodos Privados"
        private bool Validar()
        {
            if (dtmFechaNac == DateTime.Today || dtmFechaNac > DateTime.Today)
            {
                strError = "Fecha de nacimiento no válida. Por favor ingrese nuevamente.";
                return false;
            }
            if (!strDocumento.All(char.IsDigit) || !strTelefono.All(char.IsDigit))
            {
                strError = "El documento y teléfono debe contener solo números. Intente nuevamente";
                return false;
            }
            return true;
        }
        #endregion

        #region "Métodos Públicos"
        public override bool Grabar()
        {
            if (!Validar())
                return false;

            string strPath = AppDomain.CurrentDomain.BaseDirectory + @"Extension.txt";
            string strCadena = strDocumento + ";" + strNombres + ";" +
                               dtmFechaNac + ";" + strTelefono + ";" +
                               strFacultad + ";" + strCurso;

            try
            {
                using (StreamWriter Writer = new StreamWriter(strPath, true))
                {
                    Writer.WriteLine(strCadena);
                }
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return false;
            }
            return true;
        }
        #endregion
    }
}
