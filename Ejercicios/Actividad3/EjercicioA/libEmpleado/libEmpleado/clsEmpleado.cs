﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using libPersona;
using System.IO;

namespace libEmpleado
{
    public class clsEmpleado : clsPersona
    {
        #region "Atributos"
        private string strCargo;
        private Int32 intSalario;
        #endregion

        #region "Constructor"
        public clsEmpleado()
        {
            strCargo = string.Empty;
            intSalario = 0;
            //Heredados
            strDocumento = string.Empty;
            strNombres = string.Empty;
            dtmFechaNac = DateTime.Today;
            strTelefono = string.Empty;
            strError = string.Empty;
        }
        #endregion

        #region "Propiedades"
        public string Cargo
        {
            set { strCargo = value; }
            get { return strCargo; }
        }
        public Int32 Salario
        {
            set { intSalario = value; }
            get { return intSalario; }
        }
        #endregion

        #region "Métodos Privados"
        private bool Validar()
        {
            if (dtmFechaNac == DateTime.Today || dtmFechaNac > DateTime.Today)
            {
                strError = "Fecha de nacimiento no válida. Por favor ingrese nuevamente.";
                return false;
            }
            if (!strDocumento.All(char.IsDigit) || !strTelefono.All(char.IsDigit))
            {
                strError = "El documento y teléfono debe contener solo números. Intente nuevamente";
                return false;
            }
            if (intSalario < 0)
            {
                strError = "Error, el salario no debe ser menor a cero. Intente nuevamente.";
                return false;
            }
            return true;
        }
        #endregion

        #region "Métodos Públicos"
        public override bool Grabar()
        {
            if (!Validar())
                return false;

            string strPath = AppDomain.CurrentDomain.BaseDirectory + @"Empleados.txt";
            string strCadena = strDocumento + ";" + strNombres + ";" +
                               dtmFechaNac + ";" + strTelefono + ";" +
                               strCargo + ";" + intSalario;

            try
            {
                using (StreamWriter Writer = new StreamWriter(strPath, true))
                {
                    Writer.WriteLine(strCadena);
                }
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return false;
            }
            return true;
        }
        #endregion
    }
}
