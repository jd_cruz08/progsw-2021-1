﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using libPersona;

namespace libEstudiante
{
    public abstract class clsEstudiante : libPersona.clsPersona
    {
        #region "Atributos"
        protected string strFacultad;
        #endregion

        #region "Propiedades"
        public string Facultad
        {
            set { strFacultad = value; }
            get { return strFacultad; }
        }
        #endregion
    }
}
