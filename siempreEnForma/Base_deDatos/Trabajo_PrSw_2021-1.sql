CREATE DATABASE BDSiempreEnForma;
GO

--DROP DATABASE BDSiempreEnForma;

USE BDSiempreEnForma
GO

CREATE TABLE tblEstadoAparato
(
	IdEstado INT IDENTITY PRIMARY KEY NOT NULL,
	Descripcion VARCHAR(20) NOT NULL,
);
GO

CREATE TABLE tblTipoSala
(
	IdTipoSala INT IDENTITY PRIMARY KEY NOT NULL,
	Descripcion VARCHAR(20) NOT NULL,
);
GO

CREATE TABLE tblSala
(
	NroSala INT PRIMARY KEY NOT NULL,
	FK_IdTipoSala INT FOREIGN KEY REFERENCES tblTipoSala (IdTipoSala),
	Metraje FLOAT NOT NULL,
	Aparatos BIT DEFAULT 1 NOT NULL,
	Clases BIT DEFAULT 1 NOT NULL,
	Ubicacion VARCHAR(20) NOT NULL
);
GO

CREATE TABLE tblAparato
(
	IdAparato INT IDENTITY PRIMARY KEY NOT NULL,
	Descripcion VARCHAR(30) NOT NULL,
	Marca VARCHAR (20) NOT NULL,
	Modelo VARCHAR (20) NOT NULL,
	FechaCompra DATE NOT NULL,
	FK_IdEstado INT FOREIGN KEY REFERENCES tblEstadoAparato (IdEstado),
	FK_NroSala INT FOREIGN KEY REFERENCES tblSala (NroSala)
);
GO

CREATE TABLE tblTipoClase
(
	IdTipoClase INT IDENTITY PRIMARY KEY NOT NULL,
	Descripcion VARCHAR(30) NOT NULL
);
GO

CREATE TABLE tblTipoDoc
(
	IdTipoDoc VARCHAR(2) PRIMARY KEY NOT NULL,
	Descripcion VARCHAR(30) NOT NULL
);
GO

CREATE TABLE tblPersona
(
	IdPersona INT IDENTITY(1000,1) PRIMARY KEY NOT NULL,
	FK_IdTipoDoc VARCHAR(2) FOREIGN KEY REFERENCES tblTipoDoc (IdTipoDoc),
	Documento VARCHAR(15) NOT NULL,
	Nombre VARCHAR(50) NOT NULL,
	FechaNacimiento DATE NOT NULL,
	CorreoElectronico VARCHAR(60)
);
GO

CREATE TABLE tblSocio
(
	IdSocio INT IDENTITY (1000,1) NOT NULL UNIQUE,
	FK_IdPersona INT PRIMARY KEY FOREIGN KEY REFERENCES tblPersona (IdPersona),
	Profesion VARCHAR(30) NOT NULL,
	Activo BIT DEFAULT 1 NOT NULL
);
GO

CREATE TABLE tblMonitor
(
	FK_IdPersona INT PRIMARY KEY FOREIGN KEY REFERENCES tblPersona (IdPersona),
	Titulado BIT DEFAULT 1 NOT NULL,
	Experiencia TINYINT NOT NULL,
	Activo BIT DEFAULT 1 NOT NULL
);
GO

CREATE TABLE Monitor_Tiene_TipoClase
(
	FK_IdPersonaMonitor INT FOREIGN KEY REFERENCES tblPersona (IdPersona),
	FK_IdTipoClase INT FOREIGN KEY REFERENCES tblTipoClase (IdTipoClase)
);
GO

CREATE TABLE tblUsuario
(
	Usuario VARCHAR(20) NOT NULL UNIQUE,
	FK_IdPersona INT PRIMARY KEY FOREIGN KEY REFERENCES tblPersona (IdPersona),
	Clave VARCHAR(20) NOT NULL,
	UltimoIngreso DATETIME,
	Activo BIT DEFAULT 1 NOT NULL
);
GO

CREATE TABLE tblLog_User
(
	IdLog_User INT PRIMARY KEY IDENTITY NOT NULL,
	FK_IdPersonaUsuario INT FOREIGN KEY REFERENCES tblPersona (IdPersona),
	Fecha DATETIME DEFAULT GETDATE() NOT NULL,
	Descripcion VARCHAR(300) NOT NULL
);
GO

CREATE TABLE tblCiudad
(
	Ciudad VARCHAR(30) PRIMARY KEY NOT NULL,
	Departamento VARCHAR(30) NOT NULL
);
GO

CREATE TABLE tblDireccion
(
	IdDireccion INT PRIMARY KEY IDENTITY NOT NULL,
	FK_IdPersona INT FOREIGN KEY REFERENCES tblPersona (IdPersona),
	FK_Ciudad VARCHAR(30) FOREIGN KEY REFERENCES tblCiudad (Ciudad),
	Descripcion VARCHAR(60) NOT NULL
);
GO

CREATE TABLE tblTipoTel
(
	idTipoTel INT PRIMARY KEY IDENTITY NOT NULL,
	Descripcion VARCHAR(10) NOT NULL
);
GO

CREATE TABLE tblTelefono
(
	IdTelefono INT PRIMARY KEY IDENTITY NOT NULL,
	FK_IdPersona INT FOREIGN KEY REFERENCES tblPersona (IdPersona),
	FK_IdTipoTel INT FOREIGN KEY REFERENCES tblTipoTel (IdTipoTel),
	Telefono VARCHAR(10) NOT NULL
);
GO

CREATE TABLE tblClase
(
	CodClase INT IDENTITY (100,1) PRIMARY KEY  NOT NULL,
	FK_NroSala INT FOREIGN KEY REFERENCES tblSala (NroSala),
	FK_IdTipoClase INT FOREIGN KEY REFERENCES tblTipoClase (IdTipoClase),
	FK_IdPersonaMonitor INT FOREIGN KEY REFERENCES tblPersona (IdPersona),
	Hora TIME NOT NULL,
	FechaInicio DATE NOT NULL,
	FechaFin DATE NOT NULL,
	Descripcion VARCHAR(80),
	Valor MONEY NOT NULL,
	CuposDisponibles INT NOT NULL
);
GO

CREATE TABLE tblMatricula
(
	IdMatricula INT IDENTITY PRIMARY KEY NOT NULL,
	FK_IdPersonaSocio INT FOREIGN KEY REFERENCES tblPersona (IdPersona),
	Fecha DATE DEFAULT GETDATE() NOT NULL,
	ValorTotal MONEY NOT NULL,
	FK_Usuario VARCHAR(20) FOREIGN KEY REFERENCES tblUsuario (Usuario)
);
GO

CREATE TABLE tblDetalleMatricula
(
	FK_CodClase INT FOREIGN KEY REFERENCES tblClase (CodClase),
	FK_IdMatricula INT FOREIGN KEY REFERENCES tblMatricula (IdMatricula)
);
GO

/*
	======= CARGA DE DATOS INICIAL =======
*/

--DROP BOXES
INSERT INTO tblTipoDoc VALUES ( 'CC', 'C�dula de Ciudadan�a'), ( 'CE', 'C�dula de Extranjer�a'), 
	( 'PA', 'Pasaporte'), ( 'TI', 'Tarjeta de Identidad');
GO
INSERT INTO tblEstadoAparato VALUES ('Excelente'), ('Aceptable'), ('Deteriorado'), ('Deficiente');
GO
INSERT INTO tblTipoSala VALUES ('Cardio'), ('General'), ('Muscular');
GO
INSERT INTO tblTipoTel VALUES ('Fijo'), ('Celular');
GO
INSERT INTO tblTipoClase VALUES ('Aer�bic'), ('Step'), ('Stretching'), ('Spinning'), ('Xcore'), ('Zumba'), ('Crossfit');
GO
INSERT INTO tblCiudad VALUES ('Medell�n', 'Antioquia'), ('Bogot�', 'Cundinamarca'), ('Cali', 'Valle del Cauca'), 
	('Barbosa', 'Antioquia'), ('La Estrella', 'Antioquia'), ('Barranquilla', 'Atl�ntico'), ('Bucaramanga', 'Santander');
GO

--PERSONAS
INSERT INTO tblPersona VALUES 
	('CC', '1017853624', 'Juan Camilo Mu�oz Gallego',       '1995-08-15', 'juanc@gmail.com'),
	('CC', '1000212532', 'Diana Carolina Gaviria Machado',  '2000-11-10', 'dianac@gmail.com'),
	('CC', '1021213521', 'Felipe Perez Escobar',            '1999-05-01', 'felipe@gmail.com'),
	('CE', '965324',     'Mario Zambrano Smith',            '1981-05-08', 'marioz@gmail.com'),
	('PA', '1201251254', 'Hugh Michael Jackman',            '1968-10-12', 'hughm@gmail.com'),
	('TI', '1000212541', 'Ana Maria Abello',                '2004-10-04', 'anamar@gmail.com'),
	('CC', '71325412',   'Paula Andrea Jaramillo Gonzalez', '1976-07-05', 'paula@gmail.com'),
	('CC', '1021120125', 'Sandra Echeverri P�rez',          '1998-05-01', 'sandra@gmail.com'),
	('TI', '1000125412', 'Damian G�mez Ramirez',            '2003-05-01', 'damian@gmail.com'),
	('CE', '953214',     'Jer�nimo Gallego Estrada',        '1989-09-15', 'jeronimo@gmail.com'),
	('CC', '1032235624', 'Lina Marcela P�rez Aristizabal',  '1995-10-15', 'linamar@gmail.com'),
	('CC', '1017235215', 'Hernan Zapata Martinez',          '1989-09-10', 'hernanza@gmail.com'),
	('CC', '1017236521', 'Juliana Serna Gonzalez',          '1999-12-15', 'juliana@gmail.com'),
	('CC', '1017256325', 'Antonio Maya Garcia',             '1996-10-21', 'antonio@gmail.com');
GO
INSERT INTO tblTelefono VALUES
	(1000, 2, '3081730784'), (1001, 2, '3035291390'), (1002, 2, '3048366480'), (1003, 2, '3000194326'),
	(1004, 2, '3104356830'), (1005, 1, '2023645'), (1006, 2, '3045272290'), (1007, 1, '2213521'),
	(1008, 1, '2102152'), (1009, 2, '3110159402'), (1010, 2, '3174857784'), (1011, 2, '3026677896'),
	(1012, 2, '3152701390'), (1013, 2, '3152356325');
GO
INSERT INTO tblDireccion VALUES
	(1001, 'Medell�n', 'Calle 101 # 15 - 16'), (1002, 'Bogot�', 'Calle 12 # 15 - 17'),
	(1007, 'Medell�n', 'Calle 21 # 15 - 18'), (1011, 'Barbosa', 'Calle 5A # 15 - 19'),
	(1006, 'La Estrella', 'Calle 23B # 15 - 20');
GO
INSERT INTO tblSocio VALUES 
	(1003, 'Ingeniero', 1), (1004, 'Actor', 0), (1005, 'Estudiante', 1), (1006, 'Cirujana', 1),
	(1008, 'Estudiante', 1), (1009, 'Periodista', 0), (1010, 'Chef', 1), (1012, 'Ingeniera',1);
GO
INSERT INTO tblMonitor VALUES 
	(1002, 1, 1, 1), (1007, 0, 2, 1), (1001, 0, 2, 1), (1011, 1, 4, 1);
GO
INSERT INTO tblUsuario VALUES 
	('juanca15', 1000, '', '2021-06-30 15:45:21', 1), 
	('diana10', 1001, '', '2021-06-29 08:31:12', 1),
	('antonioma', 1013, '', '2021-05-12', 0);
GO

--SALAS
INSERT INTO tblSala VALUES 
	(101, 2, 30, 1, 0, 'Sur'), (102, 1, 40, 1, 1, 'Norte'), 
	(201, 3, 50, 1, 0, 'Norte'), (202, 2, 20, 0, 1, 'Sur');
GO

--APARATOS
INSERT INTO tblAparato VALUES
	('Jaula Potencia Power Rack', 'Sport Of Champions', 'LK 2021', '2021-06-15', 1, 101),
	('Bicicleta Est�tica', 'ONFITNESS', 'MT25-20', '2021-01-20', 1, 102),
	('Banca Pesas', 'Profit', 'BCO0003', '2020-11-19', 2, 201);
GO

--CLASES
INSERT INTO tblClase VALUES 
	(102, 4, 1011, '10:00', '2021-08-2', '2021-08-27', 'Quemando grasa.', 10000, 30),
	(102, 4, 1011, '14:00', '2021-08-10', '2021-09-17', 'Quemando grasa.', 10000, 30),
	(202, 1, 1002, '12:00', '2021-08-2', '2021-09-24', 'Bailemos', 7000, 20),
	(202, 5, 1002, '10:00', '2021-08-12', '2021-09-02', 'Xcore para principiantes', 15000, 17),
	(202, 3, 1001, '14:00', '2021-09-20', '2021-10-25', 'Estira esos musculos', 5000, 15);
GO

--RELACIONES
INSERT INTO Monitor_Tiene_TipoClase VALUES 
	(1002, 1), (1002, 5), (1002, 6), (1007, 7), (1001, 2), (1001, 3), (1011, 3), (1011, 4);
GO

--MATRICULA
INSERT INTO tblMatricula VALUES
	(1003, '2021-06-21', 17000, 'juanca15'), (1008, '2021-06-21', 7000, 'juanca15'),
	(1006, '2021-06-22', 15000, 'diana10'),  (1008, '2021-06-23', 25000, 'juanca15');
GO
INSERT INTO tblDetalleMatricula VALUES
	(100, 1), (102,1), (102,2), (103,3), (101, 4), (103,4);
GO

/*
	======= FUNCIONES ===============
*/
CREATE FUNCTION FC_CuposOcupados ( @CodClase INT )
RETURNS INT
AS
	BEGIN
		DECLARE @cantUsada INT = (SELECT COUNT(*) FROM tblDetalleMatricula WHERE @CodClase = FK_CodClase);
		RETURN @cantUsada;
	END
GO


/*
	======= STORED PROCEDURES =======
*/
CREATE PROCEDURE USP_Socio_BuscarXId
@IdSocio INT
AS
    BEGIN
        SELECT tblSocio.IdSocio,
               tblPersona.Nombre,
               tblPersona.Documento,
               tblSocio.Activo
          FROM tblSocio
               INNER JOIN tblPersona ON tblSocio.FK_IdPersona = tblPersona.IdPersona
        WHERE (IdSocio = @IdSocio)
    END
    --EXEC USP_Socio_BuscarXId '1008';
GO

CREATE PROCEDURE USP_Socio_BuscarXDoc
@DocSocio INT
AS
    BEGIN
        SELECT tblSocio.IdSocio,
               tblPersona.Nombre,
               tblPersona.Documento,
               tblSocio.Activo
          FROM tblSocio
               INNER JOIN tblPersona ON tblSocio.FK_IdPersona = tblPersona.IdPersona
        WHERE (tblPersona.Documento = @DocSocio)
    END
    --EXEC USP_Socio_BuscarXDoc '1201251254';
GO

CREATE PROCEDURE USP_Socio_BuscarXMatricula
@IdMatricula INT
AS
    BEGIN
        SELECT tblPersona.IdPersona,
               tblPersona.Nombre,
               tblPersona.Documento,
               tblSocio.Activo
          FROM tblSocio
               INNER JOIN tblPersona ON tblSocio.FK_IdPersona = tblPersona.IdPersona
               INNER JOIN tblMatricula ON tblSocio.FK_IdPersona = tblMatricula.FK_IdPersonaSocio
        WHERE (tblMatricula.IdMatricula = @IdMatricula)
    END
    --EXEC USP_Socio_BuscarXMatricula '1201251254';
GO

CREATE PROCEDURE USP_Clase_LlenarComboMatricula
@IdSocio INT
AS
	BEGIN
		SELECT clases.CodClase AS Clave,
		CONVERT(CHAR(15), tpClase.Descripcion)+' H '+LEFT(CONVERT (NVARCHAR, clases.Hora, 8),2) AS Dato
		FROM 
		tblClase AS clases
		LEFT JOIN tblTipoClase AS tpClase ON (clases.FK_IdTipoClase = tpClase.IdTipoClase)
		LEFT JOIN(
			SELECT FK_CodClase FROM tblMatricula AS Matricula
			LEFT JOIN tblDetalleMatricula AS detalle ON (detalle.FK_IdMatricula = Matricula.IdMatricula)
			LEFT JOIN tblSocio AS socio ON (socio.FK_IdPersona = matricula.FK_IdPersonaSocio)
			WHERE socio.IdSocio = @IdSocio
		) AS clasesIns ON (clasesIns.FK_CodClase = clases.CodClase)
		WHERE clasesIns.FK_CodClase IS NULL
	END
	--EXEC USP_Clase_LlenarComboMatricula 1003;
GO

CREATE PROCEDURE USP_Clase_BuscarXCodigo
@Codigo INT
AS
    BEGIN
        SELECT CodClase,
               FK_NroSala AS NroSala,
               tblTipoClase.Descripcion AS TipoClase,
               tblPersona.Nombre AS Monitor,
               CONVERT (NVARCHAR, Hora, 8) AS Hora,
               FechaInicio,
               FechaFin,
               tblClase.Descripcion,
               Valor,
               tblClase.CuposDisponibles - dbo.FC_CuposOcupados (CodClase) AS CuposDisponibles
          FROM tblClase
               INNER JOIN tblTipoClase ON tblClase.FK_IdTipoClase = tblTipoClase.IdTipoClase
               INNER JOIN tblPersona ON tblClase.FK_IdPersonaMonitor = tblPersona.IdPersona
        WHERE (CodClase = @Codigo);
    END
    --EXEC USP_Clase_BuscarXCodigo 102;
GO

CREATE PROCEDURE USP_Matricula_BuscarXIdPersona
@IdSocio INT
AS
    BEGIN
        SELECT IdMatricula as Matricula,
			   tblSocio.IdSocio as Socio,
               Fecha as FechaMatricula,
               ValorTotal as ValorMatricula,
               FK_Usuario as Encargado
        FROM tblMatricula
        LEFT JOIN tblSocio ON (tblsocio.FK_IdPersona = tblMatricula.FK_IdPersonaSocio)
        WHERE (tblSocio.IdSocio = @IdSocio)
    END
    --EXEC USP_Matricula_BuscarXIdPersona 1000;
GO

CREATE PROCEDURE USP_Matricula_BuscarXId
@IdMatricula INT
AS
    BEGIN
        SELECT IdMatricula as Matricula,
               tblSocio.IdSocio as Socio,
               Fecha as FechaMatricula,
               ValorTotal as ValorMatricula,
               FK_Usuario as Encargado
        FROM tblMatricula
        LEFT JOIN tblSocio ON (tblSocio.FK_IdPersona = tblMatricula.FK_IdPersonaSocio)
        WHERE (IdMatricula = @IdMatricula)
    END
    --EXEC USP_Matricula_BuscarXId 2;
    --select * from tblMatricula
GO

CREATE PROCEDURE USP_Matricula_BuscarDetalleXId
@IdMatricula INT
AS
    BEGIN
        SELECT CodClase,
               tblTipoClase.Descripcion AS TipoClase,
               CONVERT (NVARCHAR, Hora, 8) AS Hora,
               FORMAT (FechaInicio, 'dd-MM-yyyy') AS Comienzo,
               FORMAT (FechaFin, 'dd-MM-yyyy') AS Finalizacion,
               FK_NroSala AS Sala,
               Valor
          FROM tblClase
               INNER JOIN tblTipoClase ON FK_IdTipoClase = tblTipoClase.IdTipoClase
               INNER JOIN tblDetalleMatricula ON CodClase = tblDetalleMatricula.FK_CodClase
        WHERE (tblDetalleMatricula.FK_IdMatricula = @IdMatricula)
    END
    --EXEC USP_Matricula_BuscarDetalleXId 4;
GO

CREATE PROCEDURE USP_Matricula_DetalleClase
@Codigo INT
AS
	BEGIN
		SELECT CodClase AS Clase,
			   tblTipoClase.Descripcion AS Tipo,
			   LEFT(CONVERT (NVARCHAR, Hora, 8),5) AS Hora,
			   FORMAT (FechaInicio, 'dd-MM-yyyy') AS Comienzo,
			   FORMAT (FechaFin, 'dd-MM-yyyy') AS Finalizacion,
			   FK_NroSala AS Sala,
			   FORMAT (Valor, '##.##') AS Valor,
			   tblClase.CuposDisponibles - dbo.FC_CuposOcupados (CodClase) AS Cupos
		  FROM tblClase
			   INNER JOIN tblTipoClase ON tblClase.FK_IdTipoClase = tblTipoClase.IdTipoClase
			   INNER JOIN tblPersona ON tblClase.FK_IdPersonaMonitor = tblPersona.IdPersona
		WHERE (CodClase = @Codigo);
	END
	--EXEC USP_Matricula_DetalleClase 102;
GO

CREATE PROCEDURE USP_Matricula_Crear
@socio   INT,
@usuario VARCHAR(20),
@valor   MONEY
AS
	BEGIN
		DECLARE @persona INT = (SELECT FK_IdPersona FROM tblSocio WHERE @socio = IdSocio)

		INSERT INTO tblMatricula(Fecha,FK_IdPersonaSocio,FK_Usuario,ValorTotal)
		VALUES(GETDATE(),@persona,@usuario,@valor)

		SELECT CONVERT(INT,SCOPE_IDENTITY())
	END
	--EXEC USP_Matricula_Crear
GO

CREATE PROCEDURE USP_Matricula_GuardarDetalle
@matricula INT,
@clase     INT
AS
	BEGIN
		IF(EXISTS(SELECT * FROM tblMatricula WHERE @matricula = IdMatricula))
		BEGIN
			INSERT INTO tblDetalleMatricula VALUES(@clase, @matricula)
		END
	END
	--EXEC USP_Matricula_Crear
	--select * from tbldetallematricula
GO

CREATE PROCEDURE USP_Sala_LlenarComboSiAparatos
AS
	BEGIN
		SELECT NroSala AS Clave,
			   CONCAT(NroSala, ' - ', Descripcion) AS Dato
		  FROM tblSala
			   INNER JOIN tblTipoSala ON tblTipoSala.IdTipoSala = tblSala.FK_IdTipoSala
		 WHERE (Aparatos = 1)
	END
	--EXEC USP_Sala_LlenarComboSiAparatos;
GO

CREATE PROCEDURE USP_Aparato_BuscarXId
@IdAparato INT
AS
	BEGIN
		SELECT IdAparato,
			   tblEstadoAparato.Descripcion AS Estado,
			   FK_NroSala AS NroSala,
			   Marca,
			   Modelo,
			   FORMAT (FechaCompra, 'dd-MM-yyyy') AS FechaCompra,
			   tblAparato.Descripcion
		  FROM tblAparato
			   INNER JOIN tblEstadoAparato ON FK_IdEstado = tblEstadoAparato.IdEstado
		 WHERE (IdAparato = @IdAparato)
	END
	--EXEC USP_Aparato_BuscarXId 1;
GO

CREATE PROCEDURE USP_Aparato_BuscarXSala
@Sala INT
AS
	BEGIN
		SELECT IdAparato,
			   tblEstadoAparato.Descripcion AS Estado,
			   FK_NroSala AS NroSala,
			   Marca,
			   Modelo,
			   FORMAT (FechaCompra, 'dd-MM-yyyy') AS FechaCompra,
			   tblAparato.Descripcion
		  FROM tblAparato
			   INNER JOIN tblEstadoAparato ON FK_IdEstado = tblEstadoAparato.IdEstado
		 WHERE (FK_NroSala = @Sala)
	END
	--EXEC USP_Aparato_BuscarXSala 101;
GO

CREATE PROCEDURE USP_Aparato_BuscarXMarca
@Marca VARCHAR (20)
AS
	BEGIN
		SELECT IdAparato,
			   tblEstadoAparato.Descripcion AS Estado,
			   FK_NroSala AS NroSala,
			   Marca,
			   Modelo,
			   FORMAT (FechaCompra, 'dd-MM-yyyy') AS FechaCompra,
			   tblAparato.Descripcion
		  FROM tblAparato
			   INNER JOIN tblEstadoAparato ON FK_IdEstado = tblEstadoAparato.IdEstado
		 WHERE (Marca = @Marca)
	END
	--EXEC USP_Aparato_BuscarXMarca 'profit';
GO

CREATE PROCEDURE USP_Aparato_BuscarXModelo
@Modelo VARCHAR (20)
AS
	BEGIN
		SELECT IdAparato,
			   tblEstadoAparato.Descripcion AS Estado,
			   FK_NroSala AS NroSala,
			   Marca,
			   Modelo,
			   FORMAT (FechaCompra, 'dd-MM-yyyy') AS FechaCompra,
			   tblAparato.Descripcion
		  FROM tblAparato
			   INNER JOIN tblEstadoAparato ON FK_IdEstado = tblEstadoAparato.IdEstado
		 WHERE (Modelo = @Modelo)
	END
	--EXEC USP_Aparato_BuscarXModelo 'MT25-20';
GO

CREATE PROCEDURE USP_EstadoA_LlenarRadios
AS
	BEGIN
		SELECT idEstado AS Clave,
			   Descripcion AS Dato
		  FROM tblEstadoAparato
	END
	--EXEC USP_EstadoA_LlenarRadios;
GO

CREATE PROCEDURE USP_Aparato_Grabar
@Estado      INT,
@Sala        INT,
@Marca       VARCHAR(20),
@Modelo      VARCHAR(20),
@Fecha       DATE,
@Descripcion VARCHAR(30)
AS
	BEGIN
	 BEGIN TRANSACTION tx
		INSERT INTO tblAparato
		VALUES (@Descripcion, @Marca, @Modelo, @Fecha, @Estado, @Sala);
		IF ( @@ERROR > 0 )
			BEGIN
				ROLLBACK TRANSACTION tx
				SELECT 0 AS Rpta
				RETURN
		END
	 COMMIT TRANSACTION tx
	 SELECT SCOPE_IDENTITY() AS Rpta;
	END
	--EXEC USP_Aparato_Grabar 2, 102, 'Versalles', 'RK-550', '2020-05-10', 'Banda Caminadora';
GO

CREATE PROCEDURE USP_Aparato_Modificar
@IdAparato   INT,
@Estado      INT,
@Sala        INT,
@Marca       VARCHAR(20),
@Modelo      VARCHAR(20),
@Fecha       DATE,
@Descripcion VARCHAR(30)
AS
	BEGIN
		IF NOT EXISTS(SELECT * FROM tblAparato
				  WHERE IdAparato = @IdAparato)
			BEGIN
				SELECT -1 AS Rpta
				RETURN
			END
		ELSE
			BEGIN
				BEGIN TRANSACTION tx
					UPDATE tblAparato
						SET Descripcion = @Descripcion,
							Marca = @Marca,
							Modelo = @Modelo,
							FechaCompra = @Fecha,
							FK_IdEstado = @Estado,
							FK_NroSala = @Sala
					WHERE IdAparato = @IdAparato
					IF (@@ERROR > 0)
						BEGIN
							ROLLBACK TRANSACTION tx
							SELECT 0 AS Rpta
							RETURN
					END
				COMMIT TRANSACTION tc
			SELECT @IdAparato AS Rpta;
			RETURN
		END
	END
	--EXEC USP_Aparato_Modificar 1002, 3, 101, 'Versalles', 'RK-550', '2020-05-10', 'Banda Caminadora';
GO

CREATE PROCEDURE USP_User_Login
@User VARCHAR(10),
@Pass VARCHAR(10)

AS
    BEGIN
        BEGIN
            IF(SELECT Clave FROM tblUsuario WHERE Usuario = @User and Activo = 1)= ''
                UPDATE tblUsuario SET Clave = @pass WHERE Usuario = @User
        END
        
        SELECT tblPersona.Nombre,
               tblUsuario.UltimoIngreso
        FROM tblUsuario
        LEFT JOIN tblPersona ON (tblPersona.IdPersona = tblUsuario.FK_IdPersona)
        WHERE tblUsuario.Usuario = @User and tblUsuario.Clave = @Pass and Activo = 1

        BEGIN
            IF(SELECT Usuario FROM tblUsuario WHERE Usuario = @User AND Clave = @Pass and Activo = 1)=@User
                UPDATE tblUsuario SET UltimoIngreso = GETDATE() WHERE Usuario = @User
        END
    END
    --EXEC USP_User_Login 'juanca15', '123456';
    --select * from tblUsuario
    --update tblUsuario set Clave = '' where Usuario = 'juanca15'
GO

CREATE PROCEDURE USP_LogUser_CrearEvento
@Usuario VARCHAR(20),
@Desc VARCHAR(60)
AS
	BEGIN
		DECLARE @idUsuario INT = (SELECT FK_IdPersona FROM tblUsuario WHERE Usuario = @Usuario)
		BEGIN TRANSACTION tx
			INSERT INTO tblLog_User
			VALUES (@idUsuario, GETDATE(), @Desc);
			IF ( @@ERROR > 0 )
				BEGIN
					ROLLBACK TRANSACTION tx
					SELECT 0 AS Rpta
					RETURN
			END
		COMMIT TRANSACTION tx
		SELECT SCOPE_IDENTITY() AS Rpta;
	END
	--EXEC USP_LogUser_CrearEvento 'juanca15', 'EXEC USP_(...)';
GO