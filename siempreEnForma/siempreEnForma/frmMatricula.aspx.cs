﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace siempreEnForma
{
    public partial class Formulario_web12 : System.Web.UI.Page
    {
        #region "Variables Globales"
        private static string strApp;
        private static int intOpcion; //1: Buscar, 2:Agregar, 3:Modificar
        private static Double dblTotal = 0;
        private static int intSocio;

        //Elementos Formulario
        private int intIdClase, intIdSocio, intIdMatricula;
        private DataTable dt = new DataTable();
        #endregion

        #region "Métodos"
        private void Mensaje(string Texto)
        {
            this.lblMsj.Text = Texto.Trim();
            if (Texto == string.Empty)
                this.lblMsj.Visible = false;
            else
                this.lblMsj.Visible = true;
        }
        private void MensajeInfo(string Texto)
        {
            this.lblMsjInfo.Text = Texto.Trim();
            if (Texto == string.Empty)
                this.lblMsjInfo.Visible = false;
            else
                this.lblMsjInfo.Visible = true;
        }

        private void llenarComboClases()
        {
            try
            {
                Clases.clsMatricula objXX = new Clases.clsMatricula(strApp);
                if (!objXX.llenarComboClases(intIdSocio, this.ddlClase))
                {
                    Mensaje(objXX.Error);
                    return;
                }
            }
            catch (Exception ex)
            {
                Mensaje(ex.Message);
            }
            this.ddlClase.Items.Add(new ListItem("- Seleccione una clase -", "000"));
            this.ddlClase.SelectedValue = "000";
        }

        private void llenarRblBusqueda()
        {
            this.rdbTpoBusqueSocio.Items.Clear();
            this.rdbTpoBusqueSocio.Items.Add(new ListItem("ID socio", "id"));
            this.rdbTpoBusqueSocio.Items.Add(new ListItem("Documento", "doc"));
            if (intOpcion == 0)
            {
                this.rdbTpoBusqueSocio.Items.Add(new ListItem("Matricula", "matr"));
            }

        }

        private void Limpiar()
        {
            Mensaje(string.Empty);
            MensajeInfo(string.Empty);
            this.txtSocio.Text = string.Empty;
            this.rdbTpoBusqueSocio.SelectedIndex = 0;
            this.txtNombre.Text = string.Empty;
            this.txtDocumento.Text = string.Empty;
            this.chbEstado.Checked = false;
            limpiarClase();
            limpiarGrids();
        }

        private void limpiarClase()
        {
            foreach (Control cltfrm in this.pnlClase.Controls)
            {
                if (cltfrm is TextBox)
                    ((TextBox)cltfrm).Text = string.Empty;
            }
        }

        private void limpiarGrids()
        {
            ViewState["Clases"] = new DataTable();
            this.pnlClase.Visible = false;
            this.pnlMatricula.Visible = false;
            this.grvMatricula.DataSource = null;
            this.grvMatricula.DataBind();
            this.pnlDetalleMatricula.Visible = false;
            this.grvDetalleMatricula.DataSource = null;
            this.grvDetalleMatricula.DataBind();
        }

        private void buscarClase()
        {
            Mensaje(string.Empty);
            Clases.clsMatricula objXX = new Clases.clsMatricula(strApp);
            if (!objXX.buscarClase(intIdClase))
            {
                Limpiar();
                Mensaje(objXX.Error);
                objXX = null;
                return;
            }
            this.txtCodigo.Text = objXX.intIdClase.ToString();
            this.txtSala.Text = objXX.intSala.ToString();
            this.txtTipoClase.Text = objXX.strTipoClase;
            this.txtCupos.Text = objXX.intCupos.ToString();
            this.txtValor.Text = "$ " + String.Format("{0:0,0.0}", objXX.dblValor);
            this.txtMonitor.Text = objXX.strMonitor;
            this.txtHorario.Text = objXX.strHora;
            this.txtFechaIni.Text = objXX.dtFechaInicial.ToString("yyyy-MM-dd");
            this.txtFechafin.Text = objXX.dtFechaFinal.ToString("yyyy-MM-dd");
            this.txtDescripcion.Text = objXX.strDescripcion;

        }

        private void agregarClaseMatricua()
        {
            Mensaje(string.Empty);
            Clases.clsMatricula objXX = new Clases.clsMatricula(strApp);
            dt = (DataTable)ViewState["Clases"]; ;
            int nRow = 0;
            if (dt.Rows.Count > 0)
            {
                nRow = dt.Rows.Count;
                foreach (DataRow r in dt.Rows)
                {
                    if ((r.ItemArray[0]).ToString().Equals(intIdClase.ToString()))
                    {
                        break;
                    }
                    nRow--;
                }
            }
            if (nRow == 0)
            {
                if (!objXX.agregarClaseMatricula(intIdClase, dt))
                {
                    Mensaje(objXX.Error);
                    this.pnlMatricula.Visible = false;
                    objXX = null;
                    return;
                }
                ViewState["Clases"] = objXX.dt;
                calcularTotal();
                pnlDetalleMatricula.Visible = true;
                limpiarClase();
                this.grvDetalleMatricula.DataSource = objXX.dt;
                this.grvDetalleMatricula.DataBind();

            }
            else
            {
                Mensaje("Esta clase ya se agrego a la matricula");
            }

        }

        private void guardarMatricula()
        {
            Mensaje(string.Empty);
            intIdSocio = intSocio;
            Clases.clsMatricula objXX = new Clases.clsMatricula(strApp, (string)Session["user"], intIdSocio, Convert.ToDouble(txtTotal.Text));
            if (!objXX.guardarMatricula())
            {
                Limpiar();
                Mensaje(objXX.Error);
                objXX = null;
                return;
            }
            dt = (DataTable)ViewState["Clases"];
            foreach (DataRow r in dt.Rows)
            {
                objXX.guardarDetalleMatricula(Convert.ToInt32(r.ItemArray[0]));
            }
            Limpiar();
            MensajeInfo("Matricula " + objXX.intIdMatricula + " gurdada.");
        }

        private void calcularTotal()
        {
            dt = (DataTable)ViewState["Clases"];
            dblTotal = 0;
            foreach (DataRow r in dt.Rows)
            {
                dblTotal = dblTotal + Convert.ToSingle(r.ItemArray[6]);
            }
            this.txtTotal.Text = String.Format("{0:0,0.0}", dblTotal);
        }

        private void buscarSocioXId()
        {
            Mensaje(string.Empty);
            Clases.clsMatricula objXX = new Clases.clsMatricula(strApp);
            if (!objXX.buscarSocioID(intIdSocio))
            {
                Limpiar();
                Mensaje(objXX.Error);
                objXX = null;
                return;
            }
            intSocio = objXX.intIdSocio;
            this.txtSocio.Text = objXX.intIdSocio.ToString();
            this.txtNombre.Text = objXX.strNombre;
            this.txtDocumento.Text = objXX.strDocumento;
            this.chbEstado.Checked = objXX.blnEstado;

            if (intOpcion == 1)
            {
                if (!objXX.blnEstado)
                {
                    Mensaje("Socio inactivo");
                    return;
                }
                llenarComboClases();
                this.pnlClase.Visible = true;
            }
            else
            {
                buscarMatriculaXIdSocio();
            }

        }

        private void buscarSocioXDoc()
        {
            Mensaje(string.Empty);
            Clases.clsMatricula objXX = new Clases.clsMatricula(strApp);
            if (!objXX.buscarSocioDoc(intIdSocio))
            {
                Limpiar();
                Mensaje(objXX.Error);
                objXX = null;
                return;
            }
            intSocio = objXX.intIdSocio;
            this.txtSocio.Text = objXX.intIdSocio.ToString();
            this.txtNombre.Text = objXX.strNombre;
            this.txtDocumento.Text = objXX.strDocumento;
            this.chbEstado.Checked = objXX.blnEstado;

            if (intOpcion == 1)
            {
                if (!objXX.blnEstado)
                {
                    Mensaje("Socio inactivo");
                    return;
                }
                intIdSocio = objXX.intIdSocio;
                llenarComboClases();
                this.pnlClase.Visible = true;
            }
            else
            {
                buscarMatriculaXIdSocio();
            }

        }

        private void buscarMatriculaXIdSocio()
        {
            Mensaje(string.Empty);
            Clases.clsMatricula objXX = new Clases.clsMatricula(strApp);
            if (!objXX.buscarMatriculaIdSocio(intSocio, grvMatricula))
            {
                Mensaje(objXX.Error);
                this.pnlMatricula.Visible = false;
                objXX = null;
                return;
            }
            pnlMatricula.Visible = true;

        }

        private void buscarMatriculaXId()
        {
            Mensaje(string.Empty);
            Clases.clsMatricula objXX = new Clases.clsMatricula(strApp);
            if (!objXX.buscarMatriculaId(intIdSocio, grvMatricula))
            {
                Mensaje(objXX.Error);
                this.pnlMatricula.Visible = false;
                objXX = null;
                return;
            }
            intIdSocio = objXX.intIdSocio;
            buscarSocioXId();
            pnlMatricula.Visible = true;

        }

        private void buscarDetalleMatricula()
        {
            Mensaje(string.Empty);
            Clases.clsMatricula objXX = new Clases.clsMatricula(strApp);
            if (!objXX.buscarDetalleMatricula(intIdMatricula, grvDetalleMatricula))
            {
                Mensaje(objXX.Error);
                this.pnlDetalleMatricula.Visible = false;
                objXX = null;
                return;
            }
            pnlDetalleMatricula.Visible = true;
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                strApp = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                intOpcion = 0;
                llenarRblBusqueda();
                ViewState["Clases"] = dt;
                mnuOpciones_MenuItemClick(null, null);
            }
        }

        protected void mnuOpciones_MenuItemClick(object sender, MenuEventArgs e)
        {
            Mensaje(string.Empty);
            Limpiar();
            switch (this.mnuOpciones.SelectedValue)
            {
                case "opcBuscar":
                    intOpcion = 0;
                    btnAgregarClase.Visible = false;
                    txtTotal.Visible = false;
                    btnGuardarMtri.Visible = false;
                    grvDetalleMatricula.Columns[0].Visible = false;
                    lblTotal.Visible = false;
                    llenarRblBusqueda();
                    lblBuscar.Text = "Buscar:";
                    break;
                case "opcAgregar":
                    intOpcion = 1;
                    btnAgregarClase.Visible = true;
                    txtTotal.Visible = true;
                    btnGuardarMtri.Visible = true;
                    grvDetalleMatricula.Columns[0].Visible = true;
                    lblTotal.Visible = true;
                    llenarRblBusqueda();
                    lblBuscar.Text = "Socio:";
                    break;
                default:
                    Mensaje("Opción no válida.");
                    break;
            }
        }

        protected void ddlClase_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.ddlClase.SelectedValue) || this.ddlClase.SelectedValue.Equals("000"))
            {
                Mensaje("Clase seleccionada no válida.");
                limpiarClase();
                return;
            }
            Mensaje(this.ddlClase.SelectedValue);
            intIdClase = Convert.ToInt32(this.ddlClase.SelectedValue);
            buscarClase();
        }

        protected void grvDetalleMatricula_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (intOpcion == 1)
            {
                if (e.CommandName == "Select")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    dt = (DataTable)ViewState["Clases"];
                    dt.Rows.RemoveAt(index);
                    ViewState["Clases"] = dt;
                    this.grvDetalleMatricula.DataSource = dt;
                    calcularTotal();
                    this.grvDetalleMatricula.DataBind();
                }
            }
        }

        protected void btnAgregarClase_Click(object sender, EventArgs e)
        {
            try
            {
                intIdClase = int.Parse(txtCodigo.Text.Trim());
                agregarClaseMatricua();
            }
            catch (Exception ex)
            {
                Mensaje("Id de clase no valida: " + ex.Message);
            }
        }

        protected void btnGuardarMtri_Click(object sender, EventArgs e)
        {
            guardarMatricula();
        }

        protected void grvMatricula_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = this.grvMatricula.Rows[index];
                intIdMatricula = int.Parse(Server.HtmlEncode(row.Cells[1].Text));
                buscarDetalleMatricula();
            }
        }

        protected void grvDetalleMatricula_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Da formato al valor de la matricula
                e.Row.Cells[7].Text = String.Format("{0:0,0.0}", Convert.ToDouble(e.Row.Cells[7].Text));
            }
        }

        protected void grvMatricula_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Da formato al valor de la matricula
                e.Row.Cells[4].Text = String.Format("{0:0,0.0}", Convert.ToDouble(e.Row.Cells[4].Text));
                e.Row.Cells[3].Text = Convert.ToDateTime(e.Row.Cells[3].Text).ToShortDateString();
            }
        }

        protected void btnBuscarSocio_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                intIdSocio = int.Parse(txtSocio.Text.Trim());
            }
            catch (Exception ex)
            {
                Mensaje("Parametro de busqueda invalido: " + ex.Message);
            }

            switch (this.rdbTpoBusqueSocio.SelectedValue)
            {
                case "id":
                    Limpiar();
                    buscarSocioXId();
                    break;
                case "doc":
                    Limpiar();
                    buscarSocioXDoc();
                    break;
                case "matr":
                    Limpiar();
                    buscarMatriculaXId();
                    break;
                default:
                    Mensaje("Selecciones un tipo de busqueda.");
                    this.rdbTpoBusqueSocio.Focus();
                    break;
            }
        }
    }
}