﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace siempreEnForma
{
    public partial class Formulario_web11 : System.Web.UI.Page
    {
        #region "Variables Globales"
        private static string strApp;
        
        //Elementos Formulario
        private int intIdAparato, intNroSala;
        private string strDescripcion, strMarca, strModelo, strEstado;
        private DateTime dateCompra;
        #endregion

        #region "Métodos"
        private void Mensaje(string Texto)
        {
            this.lblMsj.Text = Texto.Trim();
            if (Texto == string.Empty)
                this.lblMsj.Visible = false;
            else
                this.lblMsj.Visible = true;
        }
        private void MensajeInfo(string Texto)
        {
            this.lblMsjInfo.Text = Texto.Trim();
            if (Texto == string.Empty)
                this.lblMsjInfo.Visible = false;
            else
                this.lblMsjInfo.Visible = true;
        }

        private void llenarComboSala()
        {
            try
            {
                Clases.clsAparato objXX = new Clases.clsAparato(strApp);
                if (!objXX.llenarComboSala(this.ddlSala))
                {
                    Mensaje(objXX.Error);
                    return;
                }
            }
            catch (Exception ex)
            {
                Mensaje(ex.Message);
            }
        }

        private void llenarRBEstado()
        {
            try
            {
                Clases.clsAparato objXX = new Clases.clsAparato(strApp);
                if (!objXX.llenarRadios(this.rblEstadoAparato))
                    Mensaje(objXX.Error);
                objXX = null;
                return;
            }
            catch (Exception ex)
            {
                Mensaje(ex.Message);
            }
        }

        private void Limpiar()
        {
            Mensaje(string.Empty);
            this.txtIdAparato.Text = string.Empty;
            this.rblEstadoAparato.SelectedIndex = 0;
            this.ddlSala.SelectedIndex = 0;
            this.txtMarca.Text = string.Empty;
            this.txtModelo.Text = string.Empty;
            this.txtFechaCompra.Text = DateTime.Today.ToString();
            this.txtDescripcion.Text = string.Empty;
        }

        private void Buscar(int Criterio) //1: IdAparato / 2: Sala / 3: Marca / 4: Modelo
        {
            Mensaje(string.Empty);
            this.grvDatos.Visible = false;
            try
            {
                Clases.clsAparato objXX = new Clases.clsAparato(strApp);
                switch (Criterio)
                {
                    case 1: //xIdAparato
                        if (!objXX.buscarXId(intIdAparato))
                        {
                            Limpiar();
                            Mensaje(objXX.Error);
                            objXX = null;
                            return;
                        }
                        this.txtIdAparato.Text = objXX.intIdAparato.ToString();
                        this.rblEstadoAparato.Items.FindByText(objXX.strEstado).Selected = true;
                        this.ddlSala.SelectedValue = objXX.intSala.ToString();
                        this.txtMarca.Text = objXX.strMarca;
                        this.txtModelo.Text = objXX.strModelo;
                        this.txtFechaCompra.Text = objXX.dtFechaCompra.ToString("yyyy-MM-dd");
                        this.txtDescripcion.Text = objXX.strDescripcion;
                        break;
                    case 2: //xSala
                        if (!objXX.buscarXNroSala(intNroSala, this.grvDatos))
                        {
                            Limpiar();
                            Mensaje(objXX.Error);
                            objXX = null;
                            return;
                        }
                        this.grvDatos.Visible = true;
                        break;
                    case 3: //xMarca
                        
                        if (!objXX.buscarXMarca(strMarca, this.grvDatos))
                        {
                            Limpiar();
                            Mensaje(objXX.Error);
                            objXX = null;
                            return;
                        }
                        this.grvDatos.Visible = true;
                        break;
                    default: //xModelo
                        if (!objXX.buscarXModelo(strModelo, this.grvDatos))
                        {
                            Limpiar();
                            Mensaje(objXX.Error);
                            objXX = null;
                            return;
                        }
                        this.grvDatos.Visible = true;
                        break;
                }
                objXX = null;

                if (this.grvDatos.Rows.Count == 1 && Criterio > 1)
                {
                    GridViewRow row = this.grvDatos.Rows[0];
                    this.txtIdAparato.Text = Server.HtmlDecode(row.Cells[1].Text);
                    this.rblEstadoAparato.Items.FindByText(Server.HtmlDecode(row.Cells[2].Text)).Selected = true;
                    this.ddlSala.SelectedValue = Server.HtmlDecode(row.Cells[3].Text);
                    this.txtMarca.Text = Server.HtmlDecode(row.Cells[4].Text);
                    this.txtModelo.Text = Server.HtmlDecode(row.Cells[5].Text);
                    this.txtFechaCompra.Text = Convert.ToDateTime(Server.HtmlDecode(row.Cells[6].Text)).ToString("yyyy-MM-dd");
                    this.txtDescripcion.Text = Server.HtmlDecode(row.Cells[7].Text);

                    this.grvDatos.Visible = false;
                }
                return;
            }
            catch (Exception ex)
            {
                Mensaje(ex.Message);
            }
        }

        private void Guardar()
        {
            
            try
            {
                if (this.mnuOpciones.SelectedValue.Equals("opcBuscar"))
                {
                    MensajeInfo("En el módulo buscar no hay elementos a guardar. Seleccione Agregar o Modificar para aplicar cambios.");
                    return;
                }

                strEstado = this.rblEstadoAparato.SelectedValue;
                intNroSala = Convert.ToInt32(this.ddlSala.SelectedValue);
                strMarca = this.txtMarca.Text.Trim();
                strModelo = this.txtModelo.Text.Trim();
                dateCompra = Convert.ToDateTime(this.txtFechaCompra.Text);
                strDescripcion = this.txtDescripcion.Text.Trim();

                Clases.clsAparato objXX = new Clases.clsAparato(strApp, (string)Session["user"], strEstado, intNroSala, strMarca, strModelo, dateCompra, strDescripcion);

                if (this.mnuOpciones.SelectedValue.Equals("opcAgregar"))
                {
                    objXX.intIdAparato = 0;
                    if (!objXX.grabarMaestro())
                    {
                        Mensaje(objXX.Error);
                        objXX = null;
                        return;
                    }
                    MensajeInfo("Aparato guardado con éxito bajo el ID número: " + objXX.intIdAparato);
                }
                else //Modificar
                {
                    objXX.intIdAparato = Convert.ToInt32(this.txtIdAparato.Text);
                    if (!objXX.modificarMaestro())
                    {
                        Mensaje(objXX.Error);
                        objXX = null;
                        return;
                    }
                    intIdAparato = objXX.intIdAparato;
                    objXX = null;
                    if (intIdAparato == -1)
                    {
                        Mensaje("El aparato que intenta modificar no se encuentra registrado.");
                        return;
                    }
                    else
                    {
                        if (intIdAparato == 0)
                        {
                            Mensaje("Error al procesar registro. Consultar con el Admón del sistema.");
                            return;
                        }
                        MensajeInfo( "El aparato " + intIdAparato + " ha sido modificado con éxito.");
                    }
                }
            }
            catch (Exception ex)
            {
                Mensaje(ex.Message);
            }
            
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) 
            {
                strApp = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                llenarComboSala();
                this.ddlSala.SelectedIndex = 0;
                llenarRBEstado();
                mnuOpciones_MenuItemClick(null, null);
            }
        }

        protected void grvDatos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = this.grvDatos.Rows[index];

                this.txtIdAparato.Text = Server.HtmlDecode(row.Cells[1].Text);
                this.rblEstadoAparato.Items.FindByText(Server.HtmlDecode(row.Cells[2].Text)).Selected = true;
                this.ddlSala.SelectedValue = Server.HtmlDecode(row.Cells[3].Text);
                this.txtMarca.Text = Server.HtmlDecode(row.Cells[4].Text);
                this.txtModelo.Text = Server.HtmlDecode(row.Cells[5].Text);
                this.txtFechaCompra.Text = Convert.ToDateTime(Server.HtmlDecode(row.Cells[6].Text)).ToString("yyyy-MM-dd");
                this.txtDescripcion.Text = Server.HtmlDecode(row.Cells[7].Text);
            }
        }

        protected void mnuOpciones_MenuItemClick(object sender, MenuEventArgs e)
        {
            Mensaje(string.Empty);
            MensajeInfo(string.Empty);
            this.grvDatos.Visible = false;
            switch (this.mnuOpciones.SelectedValue)
            {
                case "opcBuscar":
                    Limpiar();
                    this.PpnlFormulario.Enabled = true;
                    this.txtIdAparato.ReadOnly = false;
                    this.ibtnBuscarXAparato.Visible = true;
                    this.rblEstadoAparato.Enabled = false;
                    this.ddlSala.Enabled = true;
                    this.ibtnBuscarXSala.Visible = true;
                    this.txtMarca.ReadOnly = false;
                    this.ibtnBuscarXMarca.Visible = true;
                    this.txtModelo.ReadOnly = false;
                    this.ibtnBuscarXModelo.Visible = true;
                    this.txtFechaCompra.ReadOnly = true;
                    this.txtDescripcion.ReadOnly = true;
                    this.btnGuardar.Visible = false;
                    this.txtIdAparato.Focus();
                    break;
                case "opcAgregar":
                    Limpiar();
                    this.PpnlFormulario.Enabled = true;
                    this.txtIdAparato.ReadOnly = true;
                    this.ibtnBuscarXAparato.Visible = false;
                    this.rblEstadoAparato.Enabled = true;
                    this.ddlSala.Enabled = true;
                    this.ibtnBuscarXSala.Visible = false;
                    this.txtMarca.ReadOnly = false;
                    this.ibtnBuscarXMarca.Visible = false;
                    this.txtModelo.ReadOnly = false;
                    this.ibtnBuscarXModelo.Visible = false;
                    this.txtFechaCompra.ReadOnly = false;
                    this.txtFechaCompra.Text = DateTime.Today.ToString("yyyy-MM-dd");
                    this.txtDescripcion.ReadOnly = false;
                    this.btnGuardar.Visible = true;
                    this.txtMarca.Focus();
                    MensajeInfo("El ID Aparato se asigna de forma automática.");
                    break;
                case "opcModificar":
                    if (this.txtIdAparato.Text == string.Empty)
                    {
                        Limpiar();
                        Mensaje("Debe buscar el aparato para poderlo modificar.");
                        this.PpnlFormulario.Enabled = false;
                    }
                    else
                    {
                        this.PpnlFormulario.Enabled = true;
                        this.txtIdAparato.ReadOnly = true;
                        this.ibtnBuscarXAparato.Visible = false;
                        this.rblEstadoAparato.Enabled = true;
                        this.ddlSala.Enabled = true;
                        this.ibtnBuscarXSala.Visible = false;
                        this.txtMarca.ReadOnly = false;
                        this.ibtnBuscarXMarca.Visible = false;
                        this.txtModelo.ReadOnly = false;
                        this.ibtnBuscarXModelo.Visible = false;
                        this.txtFechaCompra.ReadOnly = false;
                        this.txtDescripcion.ReadOnly = false;
                        this.btnGuardar.Visible = true;
                        this.txtMarca.Focus();
                        MensajeInfo("El ID Aparato no se puede modificar.");
                    }
                    break;
                default:
                    Mensaje("Opción no válida.");
                    break;
            }
        }

        protected void ibtnBuscarXAparato_Click(object sender, ImageClickEventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtIdAparato.Text.Trim()))
            {
                Mensaje("Id del Aparato no válido.");
                return;
            }
            intIdAparato = Convert.ToInt32(this.txtIdAparato.Text);
            Buscar(1);
        }

        protected void ibtnBuscarXSala_Click(object sender, ImageClickEventArgs e)
        {
            intNroSala = Convert.ToInt32(this.ddlSala.SelectedValue);
            Buscar(2);
        }

        protected void ibtnBuscarXMarca_Click(object sender, ImageClickEventArgs e)
        {
            strMarca = this.txtMarca.Text;
            Buscar(3);
        }

        protected void ibtnBuscarXModelo_Click(object sender, ImageClickEventArgs e)
        {
            strModelo = this.txtModelo.Text;
            Buscar(4);
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Mensaje(string.Empty);
            MensajeInfo(string.Empty);
            switch (this.mnuOpciones.SelectedValue)
            {
                case "opcBuscar":
                    Limpiar();
                    break;
                case "opcAgregar":
                    Limpiar();
                    mnuOpciones_MenuItemClick(null, null);
                    break;
                case "opcModificar":
                    Limpiar();
                    mnuOpciones_MenuItemClick(null, null);
                    break;
                default:
                    Mensaje("Opción no válida.");
                    break;
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            Mensaje(string.Empty);
            MensajeInfo(string.Empty);
            Guardar();
        }
    }
}