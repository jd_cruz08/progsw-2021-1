﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace siempreEnForma
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        private static string strApp;

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            string user, pass;
            user = txtUsuario.Text.Trim();
            pass = txtClave.Text.Trim();
            ingresar(user, pass);
        }

        private void Mensaje(string Texto)
        {
            this.lblMsj.Text = Texto.Trim();
            if (Texto == string.Empty)
                this.lblMsj.Visible = false;
            else
                this.lblMsj.Visible = true;
        }

        private void Limpiar()
        {
            txtUsuario.Text = String.Empty;
            txtClave.Text = String.Empty;
        }

        private void ingresar(string user, string pass)
        {
            Mensaje(string.Empty);
            Clases.clsLogin objXX = new Clases.clsLogin(strApp);
            if (!objXX.login(user, pass))
            {
                Limpiar();
                Mensaje(objXX.Error);
                objXX = null;
                return;
            }
            Session["user"] = user;
            Session["nombreUsu"] = objXX.strNombre;
            Session["Fechalast"] = objXX.dtFechaLast.ToString();
            Response.Redirect("frminicio.aspx");

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                strApp = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                Mensaje(string.Empty);
                this.txtUsuario.Focus();
            }
        }
    }
}