﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="siempreEnForma.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Siempre en Forma</title>
    <link rel="shortcut icon" href="Images/logo2.png" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <style type="text/css">
         .auto-style11 {
            width: 203px;
            height: 179px;
        }
        .auto-style12 {
            width: 300px;
            margin-top: 50px;
        }
        
        .auto-style12 td{
            padding-top: 7px;
        }
        .auto-style13 {
            width: 283px;
            height: 65px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="text-start">
        <div>
            <table class="auto-style12" align="center">
                <tr>
                    <td class="text-center">
                        <img alt="logo" class="auto-style11" src="Images/logo2.png" />
                        <img alt="nombre" src="Images/nombre2.png" class="auto-style13" />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="form-label">Usuario</label>
                        <asp:TextBox ID="txtUsuario" runat="server" class="form-control"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="form-label">Clave</label>
                        <asp:TextBox ID="txtClave" runat="server" TextMode="Password" class="form-control"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="text-end">
                        <asp:Button ID="btnIngresar" runat="server" OnClick="btnIngresar_Click" Text="Ingresar" class="btn btn-primary" />
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="padding-top:30px; height:auto;">
                        <asp:Label ID="lblMsj" runat="server" Visible="False" CssClass="alert alert-danger"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        </div>
    </form>
</body>
</html>
