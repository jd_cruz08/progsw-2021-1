﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmClases.aspx.cs" Inherits="siempreEnForma.Formulario_web14" %>

<asp:Content ID="Content4" ContentPlaceHolderID="Cuerpo" runat="server">
    <table class="w-100">
            <tr>
                <td class="auto-style8"><h1>Clases</h1></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style8">
                    <asp:Menu ID="mnuOpciones" runat="server">
                        <Items>
                            <asp:MenuItem Text="Buscar" Value="opcBuscar"></asp:MenuItem>
                            <asp:MenuItem Text="Agregar" Value="opcAgregar"></asp:MenuItem>
                            <asp:MenuItem Text="Modificar" Value="opcModificar"></asp:MenuItem>
                        </Items>
                    </asp:Menu>
                </td>
                <td>
                    <table class="w-100">
                        <tr>
                            <td style="text-align: right; font-weight: bold;">Código Clase:</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style11">
                                <asp:TextBox ID="txtCodClase" runat="server" Width="288px" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                <asp:ImageButton ID="ibtnBuscarXClase" runat="server" ImageUrl="~/Images/icon-buscar.png" Width="16px"/>  
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-weight: bold;">Tipo de Clase:</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style11">
                                <asp:DropDownList ID="ddlTipoClase" runat="server" CssClass="btn btn-light dropdown-toggle"></asp:DropDownList>
                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-weight: bold;">Monitor:</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style11">
                                <asp:TextBox ID="txtMonitor" runat="server" Width="288px" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style6" style="text-align: right; font-weight: bold;">Sala:</td>
                            <td class="auto-style10" style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                <asp:DropDownList ID="ddlSala" runat="server" CssClass="btn btn-light dropdown-toggle"></asp:DropDownList>
                            </td>
                            <td class="auto-style7" style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-weight: bold;">Horario:</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style11">
                                <asp:TextBox ID="txtHorario" runat="server" Width="288px" CssClass="form-control" TextMode="Time"></asp:TextBox>
                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-weight: bold;">Fecha Inicial:</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style11">
                                <asp:TextBox ID="txtFechaInicio" runat="server" Width="288px" CssClass="form-control" TextMode="Date"></asp:TextBox>
                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-weight: bold;">Fecha Final::</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style11">
                                <asp:TextBox ID="txtFechaFinal" runat="server" TextMode="Date" Width="288px" CssClass="form-control"></asp:TextBox>

                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-weight: bold;">Cupos:</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style11">
                                <asp:TextBox ID="txtCupos" runat="server" Width="288px" CssClass="form-control" TextMode="Number"></asp:TextBox>
                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-weight: bold;">Valor:</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style11">
                                <asp:TextBox ID="txtValor" runat="server" TextMode="SingleLine" Width="288px" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding-left: 40%;">
                                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn btn-primary" />
                                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn btn-secondary"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:GridView ID="grvDatos" runat="server">
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>


            <h2 style="text-align: left"></h2>
    
    <style>
        #contMenu{
            align-content: center;
        }

        #bodyContent{
            
        }
        .auto-style6 {
            width: 20%;
            height: 27px;
        }
        .auto-style7 {
            height: 27px;
        }
        .auto-style8{
            vertical-align:top;
            width: 266px;
        }
        #Cuerpo_mnuOpciones{
            width:100%;
        }

        #Cuerpo_mnuOpciones ul{
            background:#ccc;
            width:100% !important;
            border:solid;
            border-radius:5px;
            border-color:#aaa;
        }

        #Cuerpo_mnuOpciones li{
            width:100% !important;
            height:30px;
            padding-left:10px;
        }

        #Cuerpo_mnuOpciones li a{
            color:black;
        }

        #Cuerpo_mnuOpciones li:hover{
            width:100% !important;
            height:30px;
            background:#bbb;
        }
        .auto-style10 {
            height: 27px;
            width: 298px;
        }
        .auto-style11 {
            width: 298px;
        }
    </style>
</asp:Content>
