﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmSalas.aspx.cs" Inherits="siempreEnForma.Formulario_web13" %>

<asp:Content ID="Content4" ContentPlaceHolderID="Cuerpo" runat="server">
    <table class="w-100">
            <tr>
                <td class="auto-style8"><h1>Salas</h1></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style8">
                    <asp:Menu ID="mnuOpciones" runat="server">
                        <Items>
                            <asp:MenuItem Text="Buscar" Value="opcBuscar"></asp:MenuItem>
                            <asp:MenuItem Text="Agregar" Value="opcAgregar"></asp:MenuItem>
                            <asp:MenuItem Text="Modificar" Value="opcModificar"></asp:MenuItem>
                        </Items>
                    </asp:Menu>
                </td>
                <td>
                    <table class="w-100">
                        <tr>
                            <td style="text-align: right; font-weight: bold;" class="auto-style10">Numero de Sala:</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style9">
                                <asp:TextBox ID="txtNroSala" runat="server" Width="288px" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                    <asp:ImageButton ID="ibtnBuscarXSala" runat="server" ImageUrl="~/Images/icon-buscar.png" Width="16px"/>  
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-weight: bold;" class="auto-style10">Tipo de Sala:</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style9">
                                <asp:RadioButtonList ID="rblTipoSala" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:RadioButtonList>
                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style6" style="text-align: right; font-weight: bold;">Metraje:</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style9">
                                <asp:TextBox ID="txtMetraje" runat="server" Width="288px" CssClass="form-control" TextMode="Number"></asp:TextBox>
                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-weight: bold;" class="auto-style10">Ubicación:</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style9">
                                <asp:TextBox ID="txtUbicacion" runat="server" Width="288px" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-weight: bold;" class="auto-style10">Aparatos:</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style9">
                                <asp:CheckBox ID="chkAparatos" runat="server"/> 
                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-weight: bold;" class="auto-style10">Clases:</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style9">
                                <asp:CheckBox ID="chkClases" runat="server"/>
                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding-left: 40%;">
                                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn btn-primary" />
                                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn btn-secondary"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:GridView ID="grvDatos" runat="server">
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>


            <h2 style="text-align: left"></h2>
    
    <style>
        #contMenu{
            align-content: center;
        }

        #bodyContent{
            
        }
        .auto-style6 {
            width: 20%;
            height: 27px;
        }
        .auto-style8{
            vertical-align:top;
            width: 266px;
        }
        #Cuerpo_mnuOpciones{
            width:100%;
        }

        #Cuerpo_mnuOpciones ul{
            background:#ccc;
            width:100% !important;
            border:solid;
            border-radius:5px;
            border-color:#aaa;
        }

        #Cuerpo_mnuOpciones li{
            width:100% !important;
            height:30px;
            padding-left:10px;
        }

        #Cuerpo_mnuOpciones li a{
            color:black;
        }

        #Cuerpo_mnuOpciones li:hover{
            width:100% !important;
            height:30px;
            background:#bbb;
        }
        .auto-style9 {
            width: 299px;
        }
        .auto-style10 {
            width: 20%;
        }
    </style>
</asp:Content>
