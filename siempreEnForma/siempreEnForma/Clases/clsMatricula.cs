﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Referencia
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using libConexionBD;
using System.Data;

namespace siempreEnForma.Clases
{
    public class clsMatricula
    {
        #region "Atributos / Propiedades"
        private string strApp;
        private string strSQL;
        public int intIdClase { get; set; }
        public string strTipoClase { get; set; }
        public string strMonitor { get; set; }
        public int intCupos { get; set; }
        public Double dblValor { get; set; }
        public int intSala { get; set; }
        public string strHora { get; set; }
        public DateTime dtFechaInicial { get; set; }
        public DateTime dtFechaFinal { get; set; }
        public string strDescripcion { get; set; }
        public int intIdSocio { get; set; }
        public string strNombre { get; set; }
        public string strDocumento { get; set; }
        public string strUsuario { get; set; }
        public bool blnEstado { get; set; }
        public int intIdMatricula { get; set; }
        public DataTable dt { get; set; }
        public string Error { private set; get; }
        private SqlDataReader myReader;
        #endregion

        #region "Constructor"
        public clsMatricula(string Aplicacion)
        {
            strApp = Aplicacion;
            strSQL = string.Empty;
            strUsuario = string.Empty;
            intIdSocio = 0;
            dblValor = 0;
            Error = string.Empty;
        }
        public clsMatricula(string Aplicacion, string usuario, int socio, double valor)
        {
            strApp = Aplicacion;
            strSQL = string.Empty;
            strUsuario = usuario;
            intIdSocio = socio;
            dblValor = valor;
            Error = string.Empty;
        }
        #endregion

        #region "Métodos Privados"
        private bool ValidarIdClase(int IdClase)
        {
            intIdClase = IdClase;
            if (intIdClase <= 0)
            {
                Error = "Error, el ID de la clase no es válido.";
                return false;
            }
            return true;
        }
        private bool ValidarIdSocio(int IdSocio)
        {
            intIdSocio = IdSocio;
            if (intIdSocio <= 0)
            {
                Error = "Error, el ID del socio no es válido.";
                return false;
            }
            return true;
        }
        private bool Grabar()
        {
            try
            {
                if (string.IsNullOrEmpty(strApp))
                {
                    Error = "Falta el nombre de la aplicación.";
                    return false;
                }

                clsConexionBD objCnx = new clsConexionBD(strApp);

                for (int i = 0; i < 2; i++)
                {
                    objCnx.SQL = strSQL;
                    if (!objCnx.consultarValorUnico(false))
                    {
                        Error = objCnx.Error;
                        objCnx.cerrarCnx();
                        objCnx = null;
                        return false;
                    }

                    strSQL = "EXEC USP_LogUser_CrearEvento '" + strUsuario + "', '" + strSQL.Replace("'", "|") + "';";
                }
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }
        #endregion

        #region "metodos publicos"
        public bool llenarComboClases(int intIdSocio, DropDownList Combo)
        {
            try
            {
                if (Combo == null)
                {
                    Error = "Error, no hay combo Clases para llenar.";
                    return false;
                }
                strSQL = "EXEC USP_Clase_LlenarComboMatricula " + intIdSocio + ";";
                clsGenerales objG = new clsGenerales();
                if (!objG.llenarCombo(strApp, Combo, strSQL, "Clave", "Dato"))
                {
                    Error = objG.Error;
                    objG = null;
                    return false;
                }
                objG = null;
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }
        public bool agregarClaseMatricula(int IdClase, DataTable dt)
        {
            try
            {
                strSQL = "USP_Matricula_DetalleClase " + IdClase + ";";
                clsConexionBD objCnx = new clsConexionBD(strApp);
                objCnx.SQL = strSQL;
                if (!objCnx.Consultar(false))
                {
                    Error = objCnx.Error;
                    objCnx = null;
                    return false;
                }
                myReader = objCnx.dataReader_Lleno;
                if (!myReader.HasRows)
                {
                    Error = "No existe registro con el número de sala: " + IdClase;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                myReader.Close();

                clsGenerales objG = new clsGenerales();
                GridView Grid = new GridView();
                if (!objG.llenarGrid(strApp, Grid, strSQL))
                {
                    Error = objG.Error;
                    objG = null;
                    return false;
                }
                dt.Merge((DataTable)Grid.DataSource);
                this.dt = dt;

                objG = null;
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }
        public bool buscarClase(int IdClase)
        {
            try
            {
                if (!ValidarIdClase(IdClase))
                    return false;

                strSQL = "USP_Clase_BuscarXCodigo " + IdClase + ";";
                clsConexionBD objCnx = new clsConexionBD(strApp);
                objCnx.SQL = strSQL;

                if (!objCnx.Consultar(false))
                {
                    Error = objCnx.Error;
                    objCnx = null;
                    return false;
                }

                myReader = objCnx.dataReader_Lleno;
                if (!myReader.HasRows)
                {
                    Error = "No existe registro de una clase con codigo: " + intIdClase;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }

                myReader.Read();
                intIdClase = myReader.GetInt32(0);
                intSala = myReader.GetInt32(1);
                strTipoClase = myReader.GetString(2);
                strMonitor = myReader.GetString(3);
                strHora = myReader.GetString(4);
                dtFechaInicial = myReader.GetDateTime(5);
                dtFechaFinal = myReader.GetDateTime(6);
                strDescripcion = myReader.GetString(7);
                dblValor = (myReader.GetSqlMoney(8).ToDouble());
                intCupos = myReader.GetInt32(9);
                myReader.Close();

                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }
        public bool buscarSocioID(int IdSocio)
        {
            try
            {
                if (!ValidarIdSocio(IdSocio))
                    return false;

                strSQL = "USP_Socio_BuscarXId " + IdSocio + ";";
                clsConexionBD objCnx = new clsConexionBD(strApp);
                objCnx.SQL = strSQL;

                if (!objCnx.Consultar(false))
                {
                    Error = objCnx.Error;
                    objCnx = null;
                    return false;
                }

                myReader = objCnx.dataReader_Lleno;
                if (!myReader.HasRows)
                {
                    Error = "No existe registro de un socio con codigo: " + intIdClase;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }

                myReader.Read();
                intIdSocio = myReader.GetInt32(0);
                strNombre = myReader.GetString(1);
                strDocumento = myReader.GetString(2);
                blnEstado = myReader.GetBoolean(3);
                myReader.Close();

                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }
        public bool buscarSocioDoc(int DocSocio)
        {
            try
            {
                if (!ValidarIdSocio(DocSocio))
                    return false;

                strSQL = "USP_Socio_BuscarXDoc '" + DocSocio + "';";
                clsConexionBD objCnx = new clsConexionBD(strApp);
                objCnx.SQL = strSQL;

                if (!objCnx.Consultar(false))
                {
                    Error = objCnx.Error;
                    objCnx = null;
                    return false;
                }

                myReader = objCnx.dataReader_Lleno;
                if (!myReader.HasRows)
                {
                    Error = "No existe registro de un socio con codigo: " + DocSocio;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }

                myReader.Read();
                intIdSocio = myReader.GetInt32(0);
                strNombre = myReader.GetString(1);
                strDocumento = myReader.GetString(2);
                blnEstado = myReader.GetBoolean(3);
                myReader.Close();

                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }
        public bool buscarMatriculaIdSocio(int idSocio, GridView Grid)
        {
            try
            {
                strSQL = "EXEC USP_Matricula_BuscarXIdPersona " + idSocio + ";";
                clsConexionBD objCnx = new clsConexionBD(strApp);
                objCnx.SQL = strSQL;
                if (!objCnx.Consultar(false))
                {
                    Error = objCnx.Error;
                    objCnx = null;
                    return false;
                }
                myReader = objCnx.dataReader_Lleno;
                if (!myReader.HasRows)
                {
                    Error = "No existe matriculas para el Socio: " + idSocio;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                myReader.Close();

                clsGenerales objG = new clsGenerales();
                if (!objG.llenarGrid(strApp, Grid, strSQL))
                {
                    Error = objG.Error;
                    objG = null;
                    return false;
                }
                objG = null;
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }
        public bool buscarMatriculaId(int idMatricula, GridView Grid)
        {
            try
            {
                strSQL = "EXEC USP_Matricula_BuscarXId " + idMatricula + ";";
                clsConexionBD objCnx = new clsConexionBD(strApp);
                objCnx.SQL = strSQL;
                if (!objCnx.Consultar(false))
                {
                    Error = objCnx.Error;
                    objCnx = null;
                    return false;
                }
                myReader = objCnx.dataReader_Lleno;
                if (!myReader.HasRows)
                {
                    Error = "No existe matriculas con id: " + idMatricula;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                myReader.Read();
                intIdSocio = myReader.GetInt32(1);
                myReader.Close();

                clsGenerales objG = new clsGenerales();
                if (!objG.llenarGrid(strApp, Grid, strSQL))
                {
                    Error = objG.Error;
                    objG = null;
                    return false;
                }

                objG = null;
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }
        public bool buscarDetalleMatricula(int idMatricula, GridView Grid)
        {
            try
            {
                strSQL = "EXEC USP_Matricula_BuscarDetalleXId " + idMatricula + ";";
                clsConexionBD objCnx = new clsConexionBD(strApp);
                objCnx.SQL = strSQL;
                if (!objCnx.Consultar(false))
                {
                    Error = objCnx.Error;
                    objCnx = null;
                    return false;
                }
                myReader = objCnx.dataReader_Lleno;
                if (!myReader.HasRows)
                {
                    Error = "No existe matriculas para el Socio: " + idMatricula;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                myReader.Close();

                clsGenerales objG = new clsGenerales();
                if (!objG.llenarGrid(strApp, Grid, strSQL))
                {
                    Error = objG.Error;
                    objG = null;
                    return false;
                }
                objG = null;
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }
        public bool guardarMatricula()
        {
            if (intIdSocio <= 0 || strUsuario.Equals(string.Empty) || dblValor <= 0)
            {
                Error = "Datos invalidos Para realizar la matricula";
                return false;
            }
            try
            {
                strSQL = "USP_Matricula_Crear " + intIdSocio + ", '" + strUsuario + "', " + dblValor + ";";
                clsConexionBD objCnx = new clsConexionBD(strApp);
                objCnx.SQL = strSQL;

                if (!objCnx.Consultar(false))
                {
                    Error = objCnx.Error;
                    objCnx = null;
                    return false;
                }

                myReader = objCnx.dataReader_Lleno;
                if (!myReader.HasRows)
                {
                    Error = "Error al crear la matricula. ";
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }

                myReader.Read();
                intIdMatricula = myReader.GetInt32(0);
                myReader.Close();

                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }
        public bool guardarDetalleMatricula(int idClase)
        {
            strSQL = "USP_Matricula_GuardarDetalle " + intIdMatricula + ", " + idClase + ";";
            return Grabar();
        }
        #endregion
    }
}