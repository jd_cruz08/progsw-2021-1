﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Referencia
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using libConexionBD;

namespace siempreEnForma.Clases
{
    public class clsAparato
    {
        #region "Atributos / Propiedades"
        private string strApp;
        private string strSQL;
        public string strUsuario { get; set; }
        public int intIdAparato { get; set; }
        public string strEstado { get; set; }
        public int intSala { get; set; }
        public string strMarca { get; set; }
        public string strModelo { get; set; }
        public DateTime dtFechaCompra { get; set; }
        public string strDescripcion { get; set; }
        public string Error { private set; get; }
        private SqlDataReader myReader;
        #endregion

        #region "Constructor"
        public clsAparato(string Aplicacion)
        {
            strApp = Aplicacion;
            strSQL = string.Empty;
            strUsuario = string.Empty;
            intIdAparato = 0;
            strEstado = string.Empty;
            intSala = 0;
            strMarca = string.Empty;
            strModelo = string.Empty;
            dtFechaCompra = DateTime.Today;
            strDescripcion = string.Empty;
            Error = string.Empty;
        }

        public clsAparato(string Aplicacion, string usuario, string Estado, int Sala, string Marca, string Modelo, DateTime FechaCompra, string Descripcion)
        {
            strApp = Aplicacion;
            strSQL = string.Empty;
            strUsuario = usuario;
            intIdAparato = 0;
            strEstado = Estado;
            intSala = Sala;
            strMarca = Marca;
            strModelo = Modelo;
            dtFechaCompra = FechaCompra;
            strDescripcion = Descripcion;
            Error = string.Empty;
        }
        #endregion

        #region "Métodos Privados"
        private bool ValidarId(int IdAparato)
        {
            intIdAparato = IdAparato;
            if (intIdAparato <= 0)
            {
                Error = "Error, el ID del Aparato no es válido.";
                return false;
            }
            return true;
        }
        private bool ValidarDatos()
        {
            if (dtFechaCompra > DateTime.Today)
            {
                Error = "La fecha de compra no debe ser futura. Ingrese nuevamente";
                return false;
            }

            if (strMarca.Length > 20)
            {
                Error = "La Marca no debe superar 20 caracteres.";
                return false;
            }

            if (strModelo.Length > 20)
            {
                Error = "El Modelo no debe superar 20 caracteres.";
                return false;
            }

            if (strDescripcion.Length > 20)
            {
                Error = "La descripción no debe superar los 30 caracteres.";
                return false;
            }

            if (string.IsNullOrEmpty(strMarca) || string.IsNullOrEmpty(strModelo) || string.IsNullOrEmpty(strDescripcion))
            {
                Error = "No debe haber campos en blanco. Intente nuevamente";
                return false;
            }
            return true;
        }

        private bool Grabar()
        {
            try
            {
                if (string.IsNullOrEmpty(strApp))
                {
                    Error = "Falta el nombre de la aplicación.";
                    return false;
                }

                clsConexionBD objCnx = new clsConexionBD(strApp);
                for (int i = 0; i < 2; i++)
                {
                    objCnx.SQL = strSQL;
                    if (!objCnx.consultarValorUnico(false))
                    {
                        Error = objCnx.Error;
                        objCnx.cerrarCnx();
                        objCnx = null;
                        return false;
                    }

                    if (i == 0)
                        intIdAparato = Convert.ToInt32(objCnx.vrUnico.ToString());

                    strSQL = "EXEC USP_LogUser_CrearEvento '" + strUsuario + "', '" + strSQL.Replace("'", "|") + "';";
                }

                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }
        #endregion

        #region "Métodos Públicos"
        public bool llenarComboSala(DropDownList Combo)
        {
            try
            {
                if (Combo == null)
                {
                    Error = "Error, no hay combo Sala para llenar.";
                    return false;
                }
                strSQL = "EXEC USP_Sala_LlenarComboSiAparatos;";
                clsGenerales objG = new clsGenerales();
                if (!objG.llenarCombo(strApp, Combo, strSQL, "Clave", "Dato"))
                {
                    Error = objG.Error;
                    objG = null;
                    return false;
                }
                objG = null;
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public bool llenarRadios(RadioButtonList radioBL)
        {
            strSQL = "EXEC USP_EstadoA_LlenarRadios;";
            clsGenerales objG = new clsGenerales();
            if (!objG.llenarRadioBL(strApp, radioBL, strSQL, "Clave", "Dato"))
            {
                Error = objG.Error;
                objG = null;
                return false;
            }
            objG = null;
            return true;
        }
        
        public bool buscarXId(int IdAparato)
        {
            try
            {
                if (!ValidarId(IdAparato))
                    return false;
                
                strSQL = "EXEC USP_Aparato_BuscarXId " + intIdAparato + ";";
                clsConexionBD objCnx = new clsConexionBD(strApp);
                objCnx.SQL = strSQL;
                
                if (!objCnx.Consultar(false))
                {
                    Error = objCnx.Error;
                    objCnx = null;
                    return false;
                }
                
                myReader = objCnx.dataReader_Lleno;
                if (!myReader.HasRows)
                {
                    Error = "No existe registro con el ID Aparato: " + intIdAparato;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                
                myReader.Read();
                intIdAparato = myReader.GetInt32(0);
                
                strEstado = myReader.GetString(1);
                intSala = myReader.GetInt32(2);
                strMarca = myReader.GetString(3);
                strModelo = myReader.GetString(4);
                dtFechaCompra = Convert.ToDateTime(myReader.GetString(5));
                strDescripcion = myReader.GetString(6);
                myReader.Close();
                
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public bool buscarXNroSala(int NroSala, GridView Grid)
        {
            try
            {
                strSQL = "EXEC USP_Aparato_BuscarXSala " + NroSala + ";";
                clsConexionBD objCnx = new clsConexionBD(strApp);
                objCnx.SQL = strSQL;
                if (!objCnx.Consultar(false))
                {
                    Error = objCnx.Error;
                    objCnx = null;
                    return false;
                }
                myReader = objCnx.dataReader_Lleno;
                if (!myReader.HasRows)
                {
                    Error = "No existe registro con el número de sala: " + NroSala;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                myReader.Close();

                clsGenerales objG = new clsGenerales();
                if (!objG.llenarGrid(strApp, Grid, strSQL))
                {
                    Error = objG.Error;
                    objG = null;
                    return false;
                }
                objG = null;
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public bool buscarXMarca(string Marca, GridView Grid)
        {
            try
            {
                strSQL = "EXEC USP_Aparato_BuscarXMarca '" + Marca + "';";
                clsConexionBD objCnx = new clsConexionBD(strApp);
                objCnx.SQL = strSQL;
                if (!objCnx.Consultar(false))
                {
                    Error = objCnx.Error;
                    objCnx = null;
                    return false;
                }
                myReader = objCnx.dataReader_Lleno;
                if (!myReader.HasRows)
                {
                    Error = "No existe registro con la marca: " + Marca;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                myReader.Close();
                clsGenerales objG = new clsGenerales();
                if (!objG.llenarGrid(strApp, Grid, strSQL))
                {
                    Error = objG.Error;
                    objG = null;
                    return false;
                }
                objG = null;
                
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public bool buscarXModelo(string Modelo, GridView Grid)
        {
            try
            {
                strSQL = "EXEC USP_Aparato_BuscarXModelo '" + Modelo + "';";
                clsConexionBD objCnx = new clsConexionBD(strApp);
                objCnx.SQL = strSQL;
                if (!objCnx.Consultar(false))
                {
                    Error = objCnx.Error;
                    objCnx = null;
                    return false;
                }
                myReader = objCnx.dataReader_Lleno;
                if (!myReader.HasRows)
                {
                    Error = "No existe registro con el modelo: " + Modelo;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                myReader.Close();

                clsGenerales objG = new clsGenerales();
                if (!objG.llenarGrid(strApp, Grid, strSQL))
                {
                    Error = objG.Error;
                    objG = null;
                    return false;
                }
                objG = null;
                
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public bool grabarMaestro()  //Completar
        {
            if (!ValidarDatos())
            {
                return false;
            }
            strSQL = "EXEC USP_Aparato_Grabar " + Convert.ToInt32(strEstado) + ", " + intSala + ", '" + strMarca + 
                                              "', '" + strModelo + "', '" + dtFechaCompra.ToString("yyyy-MM-dd") + "', '" + strDescripcion + "';";
            return Grabar();
        }

        public bool modificarMaestro() //Completar
        {
            if (!ValidarDatos())
            {
                return false;
            }
            strSQL = "EXEC USP_Aparato_Modificar " + intIdAparato + ", " + Convert.ToInt32(strEstado) + ", " + intSala + 
                                                 ", '" + strMarca + "', '" + strModelo + "', '" + dtFechaCompra.ToString("yyyy-MM-dd") + "', '" + strDescripcion + "';";
            return Grabar();
        }
        #endregion


    }
}