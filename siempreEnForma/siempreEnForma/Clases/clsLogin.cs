﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Referencia
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using libConexionBD;
using System.Data;
using System.Security.Cryptography;
using System.Text;

namespace siempreEnForma.Clases
{
    public class clsLogin
    {

        #region "Atributos / Propiedades"
        private string strApp;
        private string strSQL;
        private string hash = @"foxle@rn";
        public String strUser { get; set; }

        public String strPass { get; set; }
        public string strNombre { get; set; }
        public DateTime dtFechaLast { get; set; }

        public DataTable dt { get; set; }
        public string Error { private set; get; }

        private SqlDataReader myReader;
        #endregion

        public clsLogin(String strApp)
        {
            this.strApp = strApp;
            this.strUser = String.Empty;
            this.strPass = String.Empty;
            this.strNombre = String.Empty;
            this.dtFechaLast = DateTime.Now;
        }

        private void encriptar(String pass)
        {
            byte[] data = UTF8Encoding.UTF8.GetBytes(pass);
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
                using (TripleDESCryptoServiceProvider tripleDes = new TripleDESCryptoServiceProvider() { Key = keys, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
                {
                    ICryptoTransform transform = tripleDes.CreateEncryptor();
                    byte[] results = transform.TransformFinalBlock(data, 0, data.Length);
                    this.strPass = Convert.ToBase64String(results);
                }
            }
        }

        public bool login(String user, String pass)
        {
            encriptar(pass);
            //this.strPass = pass;
            try
            {
                strSQL = "USP_User_Login '" + user + "', '" + this.strPass + "';";
                clsConexionBD objCnx = new clsConexionBD(strApp);
                objCnx.SQL = strSQL;

                if (!objCnx.Consultar(false))
                {
                    Error = objCnx.Error;
                    objCnx = null;
                    return false;
                }

                myReader = objCnx.dataReader_Lleno;
                if (!myReader.HasRows)
                {
                    Error = "Usuario o clave incorrecto.";
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }

                myReader.Read();
                strNombre = myReader.GetString(0);
                dtFechaLast = myReader.GetDateTime(1);
                myReader.Close();

                return true;

            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }



        }
    }
}