﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmTipoSalas.aspx.cs" Inherits="siempreEnForma.Formulario_web18" %>
<asp:Content ID="Content4" ContentPlaceHolderID="Cuerpo" runat="server">
     <table class="w-100">
        <tr>
            <td class="auto-style10"><h1>Tipo Salas</h1></td>
            <td class="auto-style6"></td>
            <td class="auto-style13"></td>
            <td class="auto-style11"></td>
        </tr>
        <tr>
            <td class="auto-style8" rowspan="4">
                <asp:Menu ID="mnuOpciones" runat="server">
                    <Items>
                        <asp:MenuItem Text="Buscar" Value="buscar"></asp:MenuItem>
                        <asp:MenuItem Text="Agregar" Value="agregar"></asp:MenuItem>
                        <asp:MenuItem Text="Modificar" Value="modificar"></asp:MenuItem>
                    </Items>
                </asp:Menu>
            </td>
            <td class="auto-style12"><strong>ID Tipo Sala:</strong></td>
            <td class="auto-style13">
                <asp:TextBox ID="txtID" runat="server" Width="201px" CssClass="auto-style9"></asp:TextBox>
            </td>
            <td class="auto-style11">
                <asp:ImageButton ID="btnBuscar" runat="server" Height="17px" ImageUrl="~/Images/icon-buscar.png" Width="17px"/>
            </td>
        </tr>
        <tr>
            <td class="auto-style7"><strong>Descripcion:</strong></td>
            <td class="auto-style14">
                <asp:TextBox ID="txtDescripcion" runat="server" CssClass="auto-style9" Width="200px"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="text-center" colspan="3">
                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn btn-primary"/>
                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn btn-secondary"/>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:GridView ID="grvTipos" runat="server" Width="100%">
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content5" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .auto-style6 {
            width: 300px;
            height: 26px;
        }
        .auto-style7 {
            text-align: right;
            width: 300px;
        }
        .auto-style8 {
            vertical-align:top;
            width: 266px;
            top: 0px;
        }

        #Cuerpo_mnuOpciones{
            width:100%;
        }

        #Cuerpo_mnuOpciones ul{
            background:#ccc;
            width:100% !important;
            border:solid;
            border-radius:5px;
            border-color:#aaa;
        }

        #Cuerpo_mnuOpciones li{
            width:100% !important;
            height:30px;
            padding-left:10px;
        }

        #Cuerpo_mnuOpciones li a{
            color:black;
        }

        #Cuerpo_mnuOpciones li:hover{
            width:100% !important;
            height:30px;
            background:#bbb;
        }
        .auto-style9 {
            display: block;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            background-clip: padding-box;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            border-radius: .25rem;
            transition: none;
            border: 1px solid #ced4da;
            background-color: #fff;
        }

        .auto-style10 {
            vertical-align: top;
            width: 266px;
            height: 26px;
        }
        .auto-style11 {
            height: 26px;
        }
        .auto-style12 {
            text-align: right;
            width: 300px;
            height: 26px;
        }
        .auto-style13 {
            height: 26px;
            width: 26px;
        }
        .auto-style14 {
            width: 26px;
        }

    </style>
</asp:Content>
