﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmAparatos.aspx.cs" Inherits="siempreEnForma.Formulario_web11" %>

<asp:Content ID="Content4" ContentPlaceHolderID="Cuerpo" runat="server">
    <table class="w-100">
        <tr>
            <td class="auto-style8"><h1>Aparatos</h1></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style8">
                <asp:Menu ID="mnuOpciones" runat="server" OnMenuItemClick="mnuOpciones_MenuItemClick">
                    <Items>
                        <asp:MenuItem Text="Buscar" Value="opcBuscar" Selected="True"></asp:MenuItem>
                        <asp:MenuItem Text="Agregar" Value="opcAgregar"></asp:MenuItem>
                        <asp:MenuItem Text="Modificar" Value="opcModificar"></asp:MenuItem>
                    </Items>
                </asp:Menu>
            </td>
            <td>
                <asp:Panel ID="PpnlFormulario" runat="server">
                    <table class="w-100">
                        <tr>
                            <td style="text-align: right; font-weight: bold;">ID Aparato:</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style10">
                                    <asp:TextBox ID="txtIdAparato" runat="server" Width="288px" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                    <asp:ImageButton ID="ibtnBuscarXAparato" runat="server" ImageUrl="~/Images/icon-buscar.png" Width="16px" OnClick="ibtnBuscarXAparato_Click"/>  
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-weight: bold;" class="auto-style9">Estado:</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style9" colspan="2">
                                <asp:RadioButtonList ID="rblEstadoAparato" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style6" style="text-align: right; font-weight: bold;">Sala:</td>
                            <td class="auto-style12" style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                <asp:DropDownList ID="ddlSala" runat="server" CssClass="btn btn-light dropdown-toggle"></asp:DropDownList>
                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                    <asp:ImageButton ID="ibtnBuscarXSala" runat="server" ImageUrl="~/Images/icon-buscar.png" Width="16px" OnClick="ibtnBuscarXSala_Click"/>  
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-weight: bold;">Marca:</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style10">
                                <asp:TextBox ID="txtMarca" runat="server" Width="288px" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                    <asp:ImageButton ID="ibtnBuscarXMarca" runat="server" ImageUrl="~/Images/icon-buscar.png" Width="16px" OnClick="ibtnBuscarXMarca_Click"/>  
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-weight: bold;" class="auto-style13">Modelo:</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style14">
                                <asp:TextBox ID="txtModelo" runat="server" Width="288px" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style13">
                                    <asp:ImageButton ID="ibtnBuscarXModelo" runat="server" ImageUrl="~/Images/icon-buscar.png" Width="16px" OnClick="ibtnBuscarXModelo_Click"/>  
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-weight: bold;" class="auto-style13">Fecha de Compra:</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style14">
                                <asp:TextBox ID="txtFechaCompra" runat="server" TextMode="Date" Width="288px" CssClass="form-control"></asp:TextBox>

                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style13">

                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-weight: bold;">Descripción:</td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;" class="auto-style10">
                                <asp:TextBox ID="txtDescripcion" runat="server" Height="68px" TextMode="MultiLine" Width="288px" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td style="padding-left: 10px; padding-top: 3px; padding-bottom: 3px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding-left: 40%;">
                                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn btn-primary" OnClick="btnGuardar_Click" />
                                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn btn-secondary" OnClick="btnCancelar_Click"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding-top: 3px; padding-bottom: 3px;">
                                <asp:GridView ID="grvDatos" runat="server" CellPadding="3" CellSpacing="3" OnRowCommand="grvDatos_RowCommand" Width="80%">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" ShowHeader="False">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="itmSeleccionar" runat="server" CommandName="Select" CommandArgument='<%# Container.DataItemIndex %>' ImageUrl="~/Images/edit.png" Width="15px" AlternateText="Seleccionar" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="padding-left: 10%; padding-top:30px; height:auto;">
                <asp:Label ID="lblMsj" runat="server" Visible="False" CssClass="alert alert-danger"></asp:Label><br />
                <asp:Label ID="lblMsjInfo" runat="server" Visible="False" CssClass="alert alert-info"></asp:Label>
            </td>
        </tr>
    </table>
    
    <style>
        .alert{
            line-height: 60px;
        }
        .auto-style6 {
            width: 20%;
            height: 27px;
        }
        .auto-style8{
            vertical-align:top;
            width: 266px;
        }

        #Cuerpo_mnuOpciones{
            width:100%;
        }

        #Cuerpo_mnuOpciones ul{
            background:#ccc;
            width:100% !important;
            border:solid;
            border-radius:5px;
            border-color:#aaa;
        }

        #Cuerpo_mnuOpciones li{
            width:100% !important;
            height:30px;
            padding-left:10px;
        }

        #Cuerpo_mnuOpciones li a{
            color:black;
        }

        #Cuerpo_mnuOpciones li:hover{
            width:100% !important;
            height:30px;
            background:#bbb;
        }
        .auto-style9 {
            height: 29px;
        }
        .auto-style10 {
            width: 302px;
        }
        .auto-style12 {
            height: 27px;
            width: 302px;
        }
        .auto-style13 {
            height: 30px;
        }
        .auto-style14 {
            width: 302px;
            height: 30px;
        }
    </style>
</asp:Content>
