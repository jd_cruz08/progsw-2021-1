﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmSocio.aspx.cs" Inherits="siempreEnForma.Formulario_web17" %>
<asp:Content ID="Content4" ContentPlaceHolderID="Cuerpo" runat="server">
    <table class="w-100">
        <tr>
            <td><h1>Socio</h1></td>
            <td class="auto-style23">&nbsp;</td>
            <td class="auto-style20">&nbsp;</td>
            <td class="auto-style21">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td rowspan="11" class="auto-styleMenuOpciones">
                <asp:Menu ID="mnuOpciones" runat="server">
                    <Items>
                        <asp:MenuItem Text="Buscar" Value="opcBuscar"></asp:MenuItem>
                        <asp:MenuItem Text="Agregar" Value="opcAgregar"></asp:MenuItem>
                        <asp:MenuItem Text="Modificar" Value="opcModificar"></asp:MenuItem>
                    </Items>
                </asp:Menu>
            </td>
            <td class="auto-style16"><strong>Tipo Documento:</strong></td>
            <td class="auto-style20">
                <asp:DropDownList ID="ddlTipoDoc" runat="server" Width="359px" CssClass="btn btn-light dropdown-toggle">
                </asp:DropDownList>
            </td>
            <td class="auto-style21">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style10"><strong>Documento:</strong></td>
            <td class="auto-style19">
                <asp:TextBox ID="txtDoc" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td class="auto-style22"></td>
            <td class="auto-style6"></td>
        </tr>
        <tr>
            <td class="auto-style16"><strong>Nombre:</strong></td>
            <td class="auto-style20">
                <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td class="auto-style21">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style10"><strong>Correo:</strong></td>
            <td class="auto-style19">
                <asp:TextBox ID="txtCorreo" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td class="auto-style22"></td>
            <td class="auto-style6"></td>
        </tr>
        <tr>
            <td class="auto-style16"><strong>Fecha Nacimiento:</strong></td>
            <td class="auto-style20">
                <asp:TextBox ID="txtNacimiento" runat="server" CssClass="form-control" TextMode="Date"></asp:TextBox>
            </td>
            <td class="auto-style21">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style16"><strong>Profeción:</strong></td>
            <td class="auto-style20">
                <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td class="auto-style21">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style16"><strong>Activo:</strong></td>
            <td class="auto-style20">
                <asp:CheckBox ID="chbActivo" runat="server" />
            </td>
            <td class="auto-style21">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <table class="w-100">
                    <tr>
                        <td class="auto-style26"><strong>Telefono:</strong></td>
                        <td class="auto-style12">
                            <asp:TextBox ID="txtTelefono" runat="server" CssClass="form-control"></asp:TextBox>
                        </td>
                        <td class="auto-style28">
                            <asp:DropDownList ID="ddlTpoTelefono" runat="server" CssClass="btn btn-light dropdown-toggle">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnAgregarTelefono" runat="server" Text="Agregar" CssClass="btn btn-primary"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="grvTelefonos" runat="server" Width="100%">
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <table class="w-100">
                    <tr>
                        <td class="auto-style27"><strong>Direccion:</strong></td>
                        <td class="auto-style12">
                            <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-control"></asp:TextBox>
                        </td>
                        <td class="auto-style29">
                            <asp:DropDownList ID="ddlTpoDireccion" runat="server" CssClass="btn btn-light dropdown-toggle">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="Button2" runat="server" Text="Agregar" CssClass="btn btn-primary"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="grvDireccion" runat="server" Width="100%">
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="text-center" colspan="4">
                <br />
                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn btn-primary"/>
                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn btn-secondary"/>
            </td>
        </tr>
    </table>

</asp:Content>
<asp:Content ID="Content5" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .auto-style6 {
            height: 26px;
        }
        .auto-style10 {
            height: 26px;
            text-align: right;
            width: 163px;
        }
        .auto-style12 {
            width: 276px;
        }

        .auto-styleMenuOpciones{
            vertical-align:top;
            width: 266px;
        }
        #Cuerpo_mnuOpciones{
            width:100%;
        }

        #Cuerpo_mnuOpciones ul{
            background:#ccc;
            width:100% !important;
            border:solid;
            border-radius:5px;
            border-color:#aaa;
        }

        #Cuerpo_mnuOpciones li{
            width:100% !important;
            height:30px;
            padding-left:10px;
        }

        #Cuerpo_mnuOpciones li a{
            color:black;
        }

        #Cuerpo_mnuOpciones li:hover{
            width:100% !important;
            height:30px;
            background:#bbb;
        }
        .auto-style16 {
            width: 163px;
            text-align: right;
        }
        .auto-style19 {
            height: 26px;
            width: 358px;
        }
        .auto-style20 {
            width: 358px;
        }
        .auto-style21 {
            width: 60px;
        }
        .auto-style22 {
            height: 26px;
            width: 60px;
        }
        .auto-style23 {
            width: 163px;
        }
        .auto-style26 {
            width: 173px;
            text-align: right;
        }
        .auto-style27 {
            width: 174px;
            text-align: right;
        }
        .auto-style28 {
            width: 176px;
        }
        .auto-style29 {
            width: 174px;
        }
    </style>
</asp:Content>
