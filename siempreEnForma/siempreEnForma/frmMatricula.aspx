﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmMatricula.aspx.cs" Inherits="siempreEnForma.Formulario_web12" %>

<asp:Content ID="Content4" ContentPlaceHolderID="Cuerpo" runat="server">
    <table class="w-100">
        <tr>
            <td class="auto-style7"><h1>Matriculas</h1></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style7" rowspan="9">
                <asp:Menu ID="mnuOpciones" runat="server" OnMenuItemClick="mnuOpciones_MenuItemClick">
                    <Items>
                        <asp:MenuItem Text="Buscar" Value="opcBuscar" Selected="True"></asp:MenuItem>
                        <asp:MenuItem Text="Agregar" Value="opcAgregar"></asp:MenuItem>
                    </Items>
                </asp:Menu>
            </td>
            <td class="text-end">
                <asp:Panel ID="pnlSocio" runat="server">
                    <table class="w-100">
                        <tr>
                            <td class="auto-style42">
                                <asp:Label ID="lblBuscar" runat="server" Text="Buscar:" Font-Bold="True"></asp:Label>
                            </td>
                            <td class="auto-style43">
                                <asp:TextBox ID="txtSocio" runat="server" CssClass="auto-style28" Width="379px"></asp:TextBox>
                            </td>
                            <td class="auto-style41">
                                <asp:RadioButtonList ID="rdbTpoBusqueSocio" runat="server" CellSpacing="5" RepeatDirection="Horizontal" Height="27px" AutoPostBack="True">
                                </asp:RadioButtonList>
                            </td>
                            <td class="auto-style44">
                                <asp:ImageButton ID="btnBuscarSocio" runat="server" Height="19px" ImageUrl="~/Images/icon-buscar.png" Width="19px" OnClick="btnBuscarSocio_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style45"><strong>Nombre Socio:</strong></td>
                            <td class="auto-style49">
                                <asp:TextBox ID="txtNombre" runat="server" ReadOnly="True" CssClass="auto-style28" Width="380px"></asp:TextBox>
                            </td>
                            <td class="auto-style47"></td>
                            <td class="auto-style48"></td>
                        </tr>
                        <tr>
                            <td class="auto-style51"><strong>Documento:</strong></td>
                            <td class="auto-style50">
                                <asp:TextBox ID="txtDocumento" runat="server" ReadOnly="True" CssClass="auto-style28" Width="380px"></asp:TextBox>
                            </td>
                            <td class="auto-style40">&nbsp;</td>
                            <td class="auto-style20">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style51"><strong>Estado:</strong></td>
                            <td class="auto-style36">
                                <asp:CheckBox ID="chbEstado" runat="server" Enabled="False" Text="Activo" />
                            </td>
                            <td class="auto-style40">&nbsp;</td>
                            <td class="text-start">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style51">&nbsp;</td>
                            <td class="auto-style50">&nbsp;</td>
                            <td class="auto-style40">&nbsp;</td>
                            <td class="text-start">&nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="text-end">
                <asp:Panel ID="pnlClase" runat="server">
                    <table class="w-100">
                        <tr>
                            <td class="auto-style67"><strong>Clase:</strong></td>
                            <td class="text-start" colspan="3">
                                <asp:DropDownList ID="ddlClase" runat="server" CssClass="btn btn-light dropdown-toggle" AutoPostBack="True" OnSelectedIndexChanged="ddlClase_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style68"><strong>Codigo:</strong></td>
                            <td class="auto-style55">
                                <asp:TextBox ID="txtCodigo" runat="server" ReadOnly="True" CssClass="auto-style28" Width="383px"></asp:TextBox>
                            </td>
                            <td class="auto-style57"><strong>Horario</strong>:</td>
                            <td class="auto-style27">
                                <asp:TextBox ID="txtHorario" runat="server" ReadOnly="True" TextMode="Time" CssClass="auto-style28" Width="422px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style69"><strong>Tipo de Clase:</strong></td>
                            <td class="auto-style56">
                                <asp:TextBox ID="txtTipoClase" runat="server" ReadOnly="True" CssClass="auto-style28" Width="384px"></asp:TextBox>
                            </td>
                            <td class="auto-style58"><strong>Fecha Inicial:</strong></td>
                            <td class="text-start">
                                <asp:TextBox ID="txtFechaIni" runat="server" ReadOnly="True" TextMode="Date" CssClass="auto-style28" Width="422px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style67"><strong>Cupos Disponibles:</strong></td>
                            <td class="auto-style21">
                                <asp:TextBox ID="txtCupos" runat="server" ReadOnly="True" CssClass="auto-style28" Width="383px"></asp:TextBox>
                            </td>
                            <td class="auto-style59"><strong>Fecha Final:</strong></td>
                            <td class="auto-style20">
                                <asp:TextBox ID="txtFechafin" runat="server" ReadOnly="True" TextMode="Date" CssClass="auto-style28" Width="421px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style69"><strong>Valor:</strong></td>
                            <td class="auto-style56">
                                <asp:TextBox ID="txtValor" runat="server" ReadOnly="True" CssClass="auto-style28" Width="383px"></asp:TextBox>
                            </td>
                            <td class="auto-style58">&nbsp;</td>
                            <td class="text-start">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style68"><strong>Sala:</strong></td>
                            <td class="auto-style55">
                                <asp:TextBox ID="txtSala" runat="server" ReadOnly="True" CssClass="auto-style28" Width="383px"></asp:TextBox>
                            </td>
                            <td class="auto-style57"></td>
                            <td class="auto-style27"></td>
                        </tr>
                        <tr>
                            <td class="auto-style69"><strong>Monitor:</strong></td>
                            <td class="auto-style56">
                                <asp:TextBox ID="txtMonitor" runat="server" CssClass="auto-style28" ReadOnly="True" Width="383px"></asp:TextBox>
                            </td>
                            <td class="auto-style58">&nbsp;</td>
                            <td class="text-start">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style69"><strong>Descripción:</strong></td>
                            <td class="auto-style56">
                                <asp:TextBox ID="txtDescripcion" runat="server" CssClass="auto-style28" ReadOnly="True" TextMode="MultiLine" Width="385px"></asp:TextBox>
                            </td>
                            <td class="auto-style58">&nbsp;</td>
                            <td class="text-end">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style69">&nbsp;</td>
                            <td class="auto-style56">&nbsp;</td>
                            <td class="auto-style58">&nbsp;</td>
                            <td style="padding-right: 50px;">
                                <asp:Button ID="btnAgregarClase" runat="server" CssClass="btn btn-primary" OnClick="btnAgregarClase_Click" Text="Agregar" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="auto-style33">
                <asp:Panel ID="pnlMatricula" runat="server" Width="70%">
                    <div class="text-start">
                        <asp:GridView ID="grvMatricula" runat="server" OnRowCommand="grvMatricula_RowCommand" OnRowDataBound="grvMatricula_RowDataBound" Width="100%">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="itmSeleccionar" runat="server" AlternateText="Seleccionar" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Select" ImageUrl="~/Images/edit.png" Width="15px" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="text-center">
                <asp:Panel ID="pnlDetalleMatricula" runat="server" Width="70%">
                    <table class="w-100">
                        <tr>
                            <td class="auto-style47">&nbsp;</td>
                            <td class="auto-style32">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="text-start"><strong>Detalles Matricula</strong></td>
                            <td class="auto-style32">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:GridView ID="grvDetalleMatricula" runat="server" Width="100%" OnRowCommand="grvDetalleMatricula_RowCommand" OnRowDataBound="grvDetalleMatricula_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="itmSeleccionar" runat="server" CommandName="Select" CommandArgument='<%# Container.DataItemIndex %>' ImageUrl="~/Images/borrar.png" Width="15px" AlternateText="Seleccionar" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style35"></td>
                            <td class="auto-style31">
                                <asp:Label ID="lblTotal" runat="server" Text="Total:"></asp:Label></td>
                            <td class="text-end" style="text-align: right">
                                <asp:TextBox ID="txtTotal" runat="server" CssClass="auto-style29" ReadOnly="True" Width="100%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style47">&nbsp;</td>
                            <td class="auto-style32">
                                &nbsp;</td>
                            <td class="text-end">
                                <asp:Button ID="btnGuardarMtri" runat="server" CssClass="btn btn-primary" Text="Guardar" OnClick="btnGuardarMtri_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 10%; padding-top:30px; height:auto;">
                <asp:Label ID="lblMsj" runat="server" Visible="False" CssClass="alert alert-danger"></asp:Label><br />
                <asp:Label ID="lblMsjInfo" runat="server" Visible="False" CssClass="alert alert-info"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content5" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .alert{
            line-height: 60px;
        }

        .auto-style7 {
            vertical-align:top;
            width: 266px;
        }
        #Cuerpo_mnuOpciones{
            width:100%;
        }

        #Cuerpo_mnuOpciones ul{
            background:#ccc;
            width:100% !important;
            border:solid;
            border-radius:5px;
            border-color:#aaa;
        }

        #Cuerpo_mnuOpciones li{
            width:100% !important;
            height:30px;
            padding-left:10px;
        }

        #Cuerpo_mnuOpciones li a{
            color:black;
        }

        #Cuerpo_mnuOpciones li:hover{
            width:100% !important;
            height:30px;
            background:#bbb;
        }
        .auto-style28 {
            display: block;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            background-clip: padding-box;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            border-radius: .25rem;
            transition: none;
            border: 1px solid #ced4da;
            background-color: #fff;
        }
        .auto-style29 {
            display: block;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            background-clip: padding-box;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            border-radius: .25rem;
            transition: none;
            border: 1px solid #ced4da;
            margin-left: 0px;
            background-color: #fff;
        }
        
        .spaceRight{
            margin-right:10px;
        }
        .auto-style31 {
            text-align: right;
            width: 362px;
        }
        .auto-style32 {
            width: 362px;
        }
        .auto-style33 {
            height: 160px;
        }
        .auto-style35 {
            text-align: left;
            width: 826px;
        }
        .auto-style36 {
            text-align: left;
            width: 371px;
        }
        .auto-style37 {
            width: 161px;
        }
        .auto-style40 {
            width: 314px;
        }
        .auto-style41 {
            width: 314px;
            text-align: left;
            height: 8px;
        }
        .auto-style42 {
            width: 151px;
            height: 8px;
        }
        .auto-style43 {
            text-align: left;
            width: 371px;
            height: 8px;
        }
        .auto-style44 {
            text-align: left;
            height: 8px;
        }
        .auto-style45 {
            width: 151px;
            height: 26px;
        }
        .auto-style46 {
            width: 391px;
            height: 26px;
        }
        .auto-style47 {
            width: 314px;
            height: 26px;
        }
        .auto-style48 {
            text-align: left;
            height: 26px;
        }
        .auto-style49 {
            width: 371px;
            height: 26px;
        }
        .auto-style50 {
            width: 371px;
        }
        .auto-style51 {
            width: 151px;
        }
        </style>
</asp:Content>