﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace siempreEnForma
{
    public partial class frmPrincipal : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string nombreUsu, lastAcces;
            if (!IsPostBack)
            {
                nombreUsu = Session["nombreUsu"].ToString();
                lastAcces = Session["Fechalast"].ToString();
                lblUsuario.Text = nombreUsu;
                lblLastAcces.Text = lastAcces;
            }
        }

        protected void ibtnLogOut_Click(object sender, ImageClickEventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
    }
}