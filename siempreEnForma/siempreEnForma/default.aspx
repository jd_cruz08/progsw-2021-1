﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="siempreEnForma.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Siempre en Forma</title>
    <style type="text/css">
        .auto-style1 {
            width: 50%;
        }
        .auto-style2 {            
            width: 446px;
            height: 306px;
        }
        .auto-style4 {
            text-align: center;
        }
        .auto-style6 {
            width: 209px;
        }
        </style>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div style="padding-top: 5%;">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <table class="auto-style1" align="center">
                <tr>
                    <td class="text-center">
                        <img alt="logo" class="auto-style2" src="Images/nombre.png" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style6">&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Timer ID="Timer1" runat="server" Interval="4000" OnTick="Timer1_Tick"></asp:Timer>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4" colspan="2">
                        <div class="auto-style4">
                            POWERED BY <br />
                            <h3>JUAN DIEGO CRUZ &amp; FRANCO ARROYAVE</h3>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
